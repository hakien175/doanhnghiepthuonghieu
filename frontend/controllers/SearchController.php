<?php 
	namespace frontend\controllers;
	use yii\web\Controller;
	use frontend\models\News;
	use yii\web\NotFoundHttpException;
	use yii\db\Expression;

	use Yii;

	/**
	* 
	*/
	class SearchController extends Controller
	{
		
		
		 public function actionSearchnew(){ 
		      $searchnew = new News;
		      $searchnew = $searchnew->getSearch(16);
		      // echo "<pre>";
		      // print_r($searchnew);die;
		      return $this->render('searchnews',
		        [
		            'searchnew'=>$searchnew['searchnews'],
		             'pages'=>$searchnew['pages'],
		        ]
	        );
	       }
	}
?>