<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use backend\models\News;
use backend\models\Category;
use backend\models\Videos;
use backend\models\EmailNews;
use yii\db\Expression;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $slide_home = News::find(['title','image','description'])->where(['slide_home'=>1])->andWhere(['status'=>8])->andWhere(['public'=>1])->limit(5)->OrderBy('id DESC')->all();
        
        $news = News::find(['title','image','description'])->where(['status'=>8])->andWhere(['public'=>1])->andwhere(['slide_home'=>0])->limit(3)->OrderBy('id DESC')->all();
        $newshot = News::find(['title','image','description'])->where(['status'=>8])->andWhere(['public'=>1])->andwhere(['slide_home'=>0])->andwhere(['hot'=>1])->limit(5)->OrderBy('id DESC')->all();

        $newslast =  News::find()
            ->select(['title','type_news','news.created_at as created_at','category.name AS cat_name','category.slug AS cat_slug','news.slug AS slug','view','description','images','user_id'])
            ->leftJoin('category', 'category.id = news.cat_id')
            ->andWhere(['news.status'=>8])->andWhere(['news.public'=>1])
            ->limit(13)->OrderBy('news.id DESC')->asArray()->all();

        $focus = News::find(['title','image','description'])->where(['status'=>8])->andWhere(['type_focus'=>1])->andWhere(['public'=>1])->limit(10)->OrderBy('id DESC')->all();
        $viewmore = News::find(['title','image','description'])->where(['status'=>8])->andWhere(['public'=>1])->limit(12)->OrderBy('view DESC')->all();


        $newsread = new News(); 


        $readweek = $newsread->getReadweek();
        
        $readmonth = $newsread->getReadmonth();

        $ts = Category::find()->where(['show_home'=>1])->andWhere(['parent'=>5])->all();
        // $ts = new Category;
        // $ts= $ts->news(5);



       // $xh = Category::find()->where(['show_home'=>1])->andWhere(['parent'=>4])->all();
        //$nnnt = Category::find()->where(['show_home'=>1])->andWhere(['parent'=>6])->all();
        //$tnmt = Category::find()->where(['show_home'=>1])->andWhere(['parent'=>13])->all();
        //$bds = Category::find()->where(['show_home'=>1])->andWhere(['parent'=>7])->all();
        //$kt = Category::find()->where(['show_home'=>1])->andWhere(['parent'=>8])->all();
        //$pl = Category::find()->where(['show_home'=>1])->andWhere(['parent'=>9])->all();

        $cn = Category::find()->where(['show_home'=>1])->andWhere(['parent'=>11])->all();
        $xe = Category::find()->where(['show_home'=>1])->andWhere(['parent'=>15])->all();
        $ht = Category::find()->where(['show_home'=>1])->andWhere(['parent'=>17])->all();


        // $xh = Category::find()->where(['parent'=>13])->all();
        $focusvideo = News::find()->where(['status'=>8])->andWhere(['cat_id'=>68])->andWhere(['public'=>1])->limit(8)->OrderBy('id DESC')->all();

        $dndn = News::find()->where(['status'=>8])->andWhere(['cat_id'=>62])->andWhere(['public'=>1])->limit(8)->OrderBy('id DESC')->all();

        $imageleft = News::find()->where(['status'=>8])->andWhere(['cat_id'=>67])->andWhere(['public'=>1])->limit(3)->OrderBy('id DESC')->all();

        $videoleft = News::find()->where(['status'=>8])->andWhere(['cat_id'=>68])->andWhere(['public'=>1])->limit(3)->OrderBy('id DESC')->all();
        $videos = News::find()->where(['status'=>8])->andWhere(['cat_id'=>68])->andWhere(['public'=>1])->limit(12)->OrderBy('id DESC')->all();
         return $this->render('index',
            [
               'slide_home'=>$slide_home,
               'news'=>$news,
               'newshot'=>$newshot,
               'newslast'=>$newslast,
               'videos'=>$videos,
               'focus'=>$focus,
               'viewmore'=>$viewmore,
               'readweek'=>$readweek,
               'readmonth'=>$readmonth,
               'ts'=>$ts,
               //'xh'=>$xh,
               //'nnnt'=>$nnnt,
               //'tnmt'=>$tnmt,
               //'bds'=>$bds,
               //'kt'=>$kt,
               //'pl'=>$pl,
               'cn'=>$cn,
               'xe'=>$xe,
               'ht'=>$ht,
               'focusvideo'=>$focusvideo,
               'dndn'=>$dndn,
               'imageleft'=>$imageleft,
               'videoleft'=>$videoleft,
    
            ]
            );
    }

    public function actionAddemail(){
        $model = new EmailNews;
        if(Yii::$app->request->post()){
           $model->email = Yii::$app->request->post()['email'];
           if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $model->email)) {
                echo "error";
            } 
            elseif ($model->save()) {

              echo "ok";    
           }
        } 
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
