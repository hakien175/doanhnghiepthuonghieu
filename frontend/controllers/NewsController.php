<?php 
	namespace frontend\controllers;
	use yii\web\Controller;
	use yii\helpers\Html;
	use backend\models\Cat;
	use backend\models\News;
	use backend\models\Comment;
	use yii\web\NotFoundHttpException;
	use yii\db\Expression;
	use frontend\services\BaseService;

	use Yii;

	/**
	* 
	*/
	class NewsController extends Controller
	{
		public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
	
		public function actionDetail($slug)
		{

			
			$newdetail = News::findOne(['slug'=>$slug]);

			 $query = Yii::$app->db->createCommand()
            ->update('news', ['view'=>$newdetail->view+1], ['slug'=>$slug])
            ->execute();

            // var_dump($newdetail)  ; die;
            $newslast =  News::find()
                ->select(['title','type_news','news.created_at as created_at','category.name AS cat_name','category.slug AS cat_slug','news.slug AS slug','view','description','images','user_id'])
                ->leftJoin('category', 'category.id = news.cat_id')
                ->andWhere(['news.status'=>8])->andWhere(['news.public'=>1])
                ->limit(6)->OrderBy('news.id DESC')->asArray()->all();

			if($newdetail->cat_id == 68){
				$videoweek = new News();
	        	$videoweek = $videoweek->getReadweek1();
				$videomoi = News::find()->where(['status'=>8])->andWhere(['cat_id'=>68])->andWhere(['public'=>1])->OrderBy('id DESC')->limit(9)->all();
				$comment = new Comment;
				return $this->render('detail_video',[
					'video'=>$newdetail,
					'videomoi'=>$videomoi,
					'videoweek'=>$videoweek,
					'comment'=>$comment,
					'newslast'=>$newslast,
				]);

			}else{
				$newdetail1 = News::find()->where(['cat_id'=>$newdetail->cat_id])->andWhere(['status'=>8])->andWhere(['public'=>1])->orderBy('id DESC')->limit(3)->all();


				 $dndn = News::find()->where(['status'=>8])->andWhere(['cat_id'=>62])->andWhere(['public'=>1])->limit(7)->OrderBy('id DESC')->all();
				// var_dump($newhot);die();->andWhere(['public'=>1])->andWhere(['public'=>1])
				
				$newlienquan =  News::find()->where(['cat_id'=>$newdetail->cat_id])->andWhere(['status'=>8])->andWhere(['public'=>1])->orderBy(new Expression('rand()'))->limit(8)->all();

				$comment = new Comment;
				return $this->render('detail',[
					'newdetail'=>$newdetail,
					'newdetail1'=>$newdetail1,
					'dndn'=>$dndn,
					'newlienquan'=>$newlienquan,
					'comment'=>$comment,
					'newslast'=>$newslast,
					// 'captcha' => [
				 //            'class' => 'yii\captcha\CaptchaAction',
				 //        ],
				]);
			}

			
		}

 public function actionComment()
	    {	     
	       $model = new Comment();

	       if($model->load(Yii::$app->request->post())){
	       	
	       	   $email = Yii::$app->request->post()['Comment']['email'];
	            if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $email)) {
	                echo "error";
	            } 
	            else{

	             $model->status = 1;
	             $model->parent = 0;
	             $model->like_news = 0;
	             $model->created_at = date('Y-m-d H:i:s');
	    
			        if($model->save()){

			           echo "ok";
			        }

	            }

	   
	            

	    }

	  
	  }

  public function actionCommentparent()
    {
     
      $model = new Comment();

       if($model->load(Yii::$app->request->post())){
       	$email = Yii::$app->request->post()['Comment']['email'];
        if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $email)) {
            echo "error";
        } 
        else{
    		$model->status = 1;
	       	$model->like_news = 0;
	        $model->created_at = date('Y-m-d H:i:s');
	    
	        if($model->save()){
	             echo "ok";
	        }

        }

       
      
    }
    
  
  }


  public function actionAddlike($comment_id)
    {
    	 $model = Comment::findOne($comment_id);
    	

        Yii::$app->session['like'.$comment_id] = 'like';
        
  
        if(isset(Yii::$app->session['like'.$comment_id])){
        	$query = Yii::$app->db->createCommand()
            ->update('comment', ['like_news'=>$model->like_news+1], ['id'=>$comment_id])
            ->execute();
            if($query){
             	echo "ok";
           }

        }
        
        
    }

    public function actionRemovelike($comment_id)
    {
        $model = Comment::findOne($comment_id);
        
        unset(Yii::$app->session['like'.$comment_id]); 
        if(empty(Yii::$app->session['like'.$comment_id])){
        	$query = Yii::$app->db->createCommand()
            ->update('comment', ['like_news'=>$model->like_news-1], ['id'=>$comment_id])
            ->execute();
            if($query){
             	echo "ok";
           }

        }
    }


  	 
		
	}
?>