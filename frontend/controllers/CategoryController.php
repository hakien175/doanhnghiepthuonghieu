<?php 
	namespace frontend\controllers;
	use yii\web\Controller;
	use yii\helpers\Html;
	use backend\models\Category;
	use backend\models\News;
	use yii\web\NotFoundHttpException;
	use yii\db\Expression;
	use yii\data\Pagination;
	use Yii;

	/**
	* 
	*/
	class CategoryController extends Controller
	{
		
		
		public function actionIndex($slug)
		{
			$cat = Category::findOne(['slug'=>$slug]);

				 $value1[] = $cat->id;

				if ($cat) {
					$catss1 = Category::find()->where(['parent'=>$cat->id])->all();
					foreach ($catss1 as $item1) {
						$value1[] = $item1->id;
					}		   	

					// lay 5 tin moi nhat
					$cate_new = News::find()->Where(['cat_id'=>$value1])->andWhere(['status'=>8])->andWhere(['public'=>1])->orderBy('id DESC')->limit(5)->all();

					//lay 6 bai ngau nhien cung chuyen muc trong 20 ngay
					$today = date('Y-m-d');
					$week = strtotime(date("Y-m-d", strtotime($today)) . " -20 day");
					$cate_rand = News::find()->Where(['cat_id'=>$value1])->andWhere(['status'=>8])->andWhere(['public'=>1])->andWhere(['>', 'created_at', '$week'])->orderBy('rand()')->limit(8)->all();

					// lay danh sach tin moi bo qua 5 tin dau
					$list_post = News::find()->Where(['cat_id'=>$value1])->andWhere(['status'=>8])->andWhere(['public'=>1])->andWhere(['<', 'id', $cate_new[4]->id])->orderBy('id DESC');
					$pages = new Pagination(['totalCount' => $list_post->count(),'defaultPageSize'=>13]);
					$list_post = $list_post->offset($pages->offset)->limit($pages->limit)->all();

					$eventnews = News::find()->Where(['cat_id'=>$value1])->andWhere(['status'=>8])->andWhere(['public'=>1])->orderBy('id DESC')->limit(15)->all();
					$dndn = News::find()->where(['status'=>8])->andWhere(['cat_id'=>62])->andWhere(['public'=>1])->limit(7)->OrderBy('id DESC')->all();
				
					return $this->render('index',[
						'cate_new'=>$cate_new,
						'cat'=>$cat,
						'list_post'=>$list_post,
						'pages'=>$pages,
						'eventnews'=>$eventnews,
						'dndn'=>$dndn,
						'cate_rand'=>$cate_rand,
					]);
				}
			  

				
			

		}
			
	}
?>