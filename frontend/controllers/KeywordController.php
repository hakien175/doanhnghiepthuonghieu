<?php 
	namespace frontend\controllers;
	use yii\web\Controller;
	use yii\helpers\Html;
	use backend\models\Cat;
	use backend\models\News;
	use backend\models\NewKeyword;
	use yii\web\NotFoundHttpException;
	use yii\db\Expression;
	use yii\data\Pagination;

	use Yii;

	/**
	* 
	*/
	class KeywordController extends Controller
	{
		
		 public function actionIndex($id){
		 	$data = NewKeyword::find()->where(['keywords_id'=>$id])->all();
		 	foreach ($data as $datas) {
		 		$news_id[] = $datas->news_id;
		 	}
		 	$newdata = News::find()->where(['id'=>$news_id])->andWhere(['status'=>8])->andWhere(['public'=>1]);	
		 	  $count = $newdata->count();
	          $pages = new Pagination(['totalCount' => $count,'defaultPageSize' => 10]);
	          $newdata = $newdata->offset($pages->offset)
	        ->limit($pages->limit)
	        ->all();

	            $data = [
	                'pages' =>$pages,
	                'newdata' =>$newdata
	            ];



		 
		 	 $dndn = News::find()->where(['status'=>8])->andWhere(['cat_id'=>62])->andWhere(['public'=>1])->limit(7)->OrderBy('id DESC')->all();
		 	 $newsread = new News(); 
	         $readweek = $newsread->getReadweek();
	         $readmonth = $newsread->getReadmonth(); 	
		 	return $this->render('index',
		 		[
		 		  'newdata' => $data['newdata'],
                  'pages' => $data['pages'],
		 		  'readweek'=>$readweek,
		 		  'readmonth'=>$readmonth,
		 		  'dndn'=>$dndn,
		 		]);
		 }
		
	}
?>