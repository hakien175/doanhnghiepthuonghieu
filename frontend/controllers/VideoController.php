<?php 
	namespace frontend\controllers;
	use yii\web\Controller;
	use yii\helpers\Html;
	use backend\models\Category;
	use backend\models\News;
	use yii\web\NotFoundHttpException;
	use yii\db\Expression;
	use yii\data\Pagination;

	use Yii;

	/**
	* 
	*/
	class VideoController extends Controller
	{
		
		public function actionIndex(){
		 $home = News::find()->where(['status'=>8])->andWhere(['public'=>1])->andWhere(['cat_id'=>68])->OrderBy('id DESC');
		  $count = $home->count();
          $pages = new Pagination(['totalCount' => $count,'PageSize' => 12]);
          $home = $home->offset($pages->offset)
          ->limit($pages->limit)
          ->all();

            $data = [
                'pages' =>$pages,
                'home' =>$home
            ];
        $videoweek = new News();


        $videoweek = $videoweek->getReadweek1();
			return $this->render('index',
				[
				  'home' => $data['home'],
                  'pages' => $data['pages'],
                  'videoweek' => $videoweek,
				]);
		}
		public function actionDetail($slug)
		{
			$videoweek = new News(); 
        	$videoweek = $videoweek->getReadweek1();
			$video = News::findOne(['slug'=>$slug]);
			$videomoi = News::find()->where(['status'=>1])->andWhere(['public'=>1])->OrderBy('id DESC')->limit(4)->all();
			
			return $this->render('detail',[
				'video'=>$video,
				'videomoi'=>$videomoi,
				'videoweek'=>$videoweek,
			]);
		}
		
	}
?>