$(document).ready(function () {
    /*display weather*/
    $(".box-city").on("click", function () {
        if ($(this).hasClass("active")) {
            return false;
        } else {
            $(".box-city,.box-temp").removeClass("active");
            $(this).addClass("active")
            var nameCity = $(this).text();
            var dataCity = $(this).data("city");
            /*get name city current and display*/
            $(".name-city").text(nameCity);
            /*get temp city current and display*/
            $(".box-temp[data-temp='" + dataCity + "']").addClass("active");
        }
    })
    /*open sub menu mobile*/
    $(".button-open-menu,.js-mb-filter,.js-mb-menu-close").on("click", function () {
        $(".js-mb-filter,.js-menu-mobile").toggleClass("show");
        $('body').toggleClass("no-scroll");
    });
    $(".open-sub-menu").on("click", function () {
        $(this).toggleClass("show").siblings(".sub-menu").slideToggle();
    });
    /*backtop*/
    if ($(window).scrollTop() > 300) {
        $("#back-top").fadeIn("slow");
    } else {
        $("#back-top").fadeOut("slow");
    }
    $(window).scroll(function () {
        /*handling backtop*/
        var rangeToTop = $(this).scrollTop();
        if (rangeToTop > 300) {
            $("#back-top").fadeIn("slow");
        } else {
            $("#back-top").fadeOut("slow");
        }
        /*end*/
        /*handling fixtop menu*/
        if (rangeToTop >= $(".header_top").outerHeight() + $(".header_logo").outerHeight()) {
            $(".menu-desktop").addClass("fixtop");
        } else {
            $(".menu-desktop").removeClass("fixtop");
        }
        /*end*/
    });
    $("#back-top").click(function () {
        $("html, body").animate({scrollTop: 0}, 1000);
    });
    /*open menu*/
    $(".menu .open-menu").on("click", function () {
        $(".nav-menu").slideToggle();
    });
    $(".wd-cat .open-menu").on("click", function () {
        $(this).siblings(".sub-menu").slideToggle();
    });
    $(".open-sub").on("click", function () {
        $(this).siblings(".sub-menu").slideToggle();
    });
    appendSlider('video-home', {
        loop: true,
        items: 5,
        nav: false,
        dots: true,
        margin: 15,
        autoplay: true,
        autoplayTimeout: 6000,
        smartSpeed: 800,
        responsive: {
            0: {
                items: 2,
            },
            500: {
                items: 3,
            },
            767: {
                items: 4,
            },
            991: {
                items: 5,
            }
        }
    });
    appendSlider('slide-articles', {
        loop: true,
        items: 1,
        nav: false,
        dots: true,
        autoplay: true,
        autoplayTimeout: 10000,
        smartSpeed: 800,
    });
    // nav home video.
    $('.btn-nav_home-video-left').on('click', function () {
        $('.js-video-home-slider').trigger('prev.owl');
    });
    $('.btn-nav_home-video-right').on('click', function () {
        $('.js-video-home-slider').trigger('next.owl');
    });
    // nav slide articles.
    $('.btn-nav-slide-articles-left').on('click', function () {
        $('.js-slide-articles-slider').trigger('prev.owl');
    });
    $('.btn-nav-slide-articles-right').on('click', function () {
        $('.js-slide-articles-slider').trigger('next.owl');
    });
});
// owl carousel appending function
const appendSlider = (sliderName = '', option = {}) => {
    const slider = $('.js-' + sliderName + '-slider');
    if (slider.length) {
        slider.owlCarousel(option);
        $('.js-' + sliderName + '-slider-prev').on('click', function () {
            $(this).siblings('.js-' + sliderName + '-slider').trigger('prev.owl');
        });
        $('.js-' + sliderName + '-slider-next').on('click', function () {
            $(this).siblings('.js-' + sliderName + '-slider').trigger('next.owl');
        });
    }
};