<?php

use app\components\widgets\ViewCateHomeWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\AdsWidget;
use frontend\services\BaseService;

$this->title = "Doanh Nghiệp và thương hiệu ";
$this->registerMetaTag(['property' => 'og:description', 'content' => \Yii::$app->params['home_description']]);
$this->registerMetaTag(['property' => 'og:title', 'content' => \Yii::$app->params['home_title']]);
$this->registerMetaTag(['name' => 'keywords', 'content' => \Yii::$app->params['home_keyword']]);
$this->registerMetaTag(['property' => 'og:locale', 'content' => \Yii::$app->params['home_locale']]);
$this->registerMetaTag(['property' => 'og:type', 'content' => \Yii::$app->params['home_type']]);
$this->registerMetaTag(['property' => 'og:url', 'content' => \Yii::$app->params['baseUrl']]);
$this->registerMetaTag(['property' => 'og:site_name', 'content' => \Yii::$app->params['home_sitename']]);
?>
 <?php
    if (BaseService::detectMobile() == "desktop") {
        $text_length = 90;
    }else{
        $text_length = 150;
    }
?>
<section class="article-main mb-20">
    <div class="container">
        <div class="row gutter-15 wrap">
            <div class="col-md-9 cus-col9">
                <div class="row gutter-10 mb-30">
                    <div class="col-md-8 cus-col8">
                        <?php if (isset($newslast[0])): ?>
                            <div class="article-main_big">
                                <a href="<?= Url::to(['news/detail', 'slug1' => $newslast[0]['slug']]) ?>"
                                   class="d-block mb-2">
                                    <?= Html::img(\Yii::$app->params['mediaUrl'] . $newslast[0]["images"], ['alt' => $newslast[0]['title'], 'class' => 'w-100']) ?>
                                </a>
                                <h3 class="f-roboto-b t-20 mb-10"><a
                                            href="<?= Url::to(['news/detail', 'slug1' => $newslast[0]['slug']]) ?>"
                                            class="link_unstyle"><?= BaseService::SplitText($newslast[0]['title'],$text_length)?></a></h3>
                                <div class="sapo max-line max-line-3">
                                    <?= $newslast[0]['description'] ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-4 cus-col4">
                        <ul class="nav nav-tab_article ispc" role="tablist">
                            <?php if ($focus): ?>
                                <li>
                                    <a class="active f-roboto-b text-uppercase d-block" data-toggle="tab"
                                       href="#tieu_diem">Tiêu
                                        điểm</a>
                                </li>
                            <?php endif; ?>
                            <?php if ($readmonth): ?>
                                <li>
                                    <a class="f-roboto-b text-uppercase d-block" data-toggle="tab" href="#doc_nhieu">Đọc
                                        nhiều</a>
                                </li>
                            <?php endif; ?>
                        </ul>
                        <div class="tab-content">
                            <?php if ($focus): ?>
                                <div id="tieu_diem" class="tab-pane active">
                                    <ul class="list-post-global list-post">
                                        <?php 
                                        $i=0;
                                        foreach ($focus as $item): ?>
                                            <li class="mb-10 <?= ($i>4)? 'ispc' : ''?>"><a
                                                        href="<?= Url::to(['news/detail', 'slug1' => $item['slug']]) ?>"
                                                        class="link_unstyle"><?= $item['title'] ?></a></li>
                                        <?php $i++; endforeach; ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                            <?php if ($readmonth): ?>
                                <div id="doc_nhieu" class="tab-pane fade">
                                    <ul class="list-post-global list-post">
                                        <?php foreach ($readmonth as $item): ?>
                                            <li class="mb-10"><a
                                                        href="<?= Url::to(['news/detail', 'slug1' => $item['slug']]) ?>"
                                                        class="link_unstyle"><?= $item['title'] ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php if (isset($newslast[1])): ?>
                    <div class="row gutter-10 range-20 list-hot">
                        <?php foreach ($newslast as $k => $item): if ($k > 0 && $k < 6): ?>
                            <div class="col-sm-4 col-md mb-10">
                                <a class="d-block mb-10"
                                   href="<?= Url::to(['news/detail', 'slug1' => $item['slug']]) ?>">
                                    <?= Html::img(\Yii::$app->params['mediaUrl'] . $item["images"], ['alt' => $item['title'], 'class' => 'w-100']) ?>
                                </a>
                                <h3 class="f-roboto-b t-14 max-line max-line-2"><a
                                            href="<?= Url::to(['news/detail', 'slug1' => $item['slug']]) ?>"
                                            class="link_unstyle"><?= $item['title'] ?></a></h3>
                            </div>
                        <?php elseif ($k == 6):break; ?>
                        <?php endif;endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-md-3 cus-col3">
                <div class="p-1 border mb-2">
                    <?= AdsWidget::widget(["width"=>"300","height"=>"250", "id" => 2]) ?>
                </div>
                <div class="p-1 border mb-2 d-none d-md-block">
                    <a href="https://mbbank.com.vn/home" class="d-block text-center"><img src="http://mediadnth.90phut.vn/uploads/quangcao/m.jpg" style="width:300px; height:380px"></a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if ($videos): ?>
    <section class="video-home mb-15">
        <div class="container">
            <div class="video-home_inner">
                <div class="video-home_title">
                    <h2 class="f-roboto-b t-14 d-inline-flex align-items-center">
                        <img src="../images/icon-video.png" alt="" class="d-inline-block mr-2">Video
                    </h2>
                    <div class="d-flex nav-video-home">
                        <button class="btn-nav-video btn-nav-video-left btn-nav_home-video-left"></button>
                        <button class="btn-nav-video btn-nav-video-right btn-nav_home-video-right"></button>
                    </div>
                </div>
                <div class="owl-carousel js-video-home-slider">
                    <?php foreach ($videos as $item): ?>
                        <div class="item-video">
                            <a href="<?= Url::to(['/news/detail', 'slug1' => $item['slug']]); ?>" class="d-block mb-10 position-relative">
                                <img alt="" src="../images/icon-video_2.png" class="icon-video">
                                <?= Html::img(\Yii::$app->params['mediaUrl'] . $item["images"], ['alt' => $item['title']]) ?>
                            </a>
                            <h3 class="t-14 f-roboto-b"><a href="<?= Url::to(['/news/detail', 'slug1' => $item['slug']]); ?>" class="link_unstyle"><?= BaseService::SplitText($item['title'],90)?> <i class="fa fa-video-camera cl-999999"
                                                          aria-hidden="true"></i></a></h3>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<section class="section-cat-article mb-30">
    <div class="container">
        <div class="section-cat-article_inner">
            <div class="row gutter-10">
                <div class="col-lg-9">
                    <div class="row gutter-10">
                        <div class="col-lg-5 home-cus-col5">
                            <div class="left">
                                <?php if (isset($newslast[6])): ?>
                                    <ul class="list-article mb-15">
                                        <?php foreach ($newslast as $k => $item): if ($k > 5): ?>
                                            <li>
                                                <h3 class="t-16 f-roboto-b mb-10"><a
                                                            href="<?= Url::to(['news/detail', 'slug1' => $item['slug']]) ?>"
                                                            class="link_unstyle"><?= BaseService::SplitText($item['title'],90)?></a></h3>
                                                <div class="media">
                                                    <a href="<?= Url::to(['news/detail', 'slug1' => $item['slug']]) ?>">
                                                        <?= Html::img(\Yii::$app->params['mediaUrl'] . $item["images"], ['alt' => $item['title'], 'class' => 'mr-3', 'width' => 140]) ?>
                                                    </a>
                                                    <div class="media-body">
                                                        <div class="cl-7f7f7f mb-1"><?= $item['created_at'] ?> (GMT+7)</div>
                                                        <div class="des max-line max-line-3 mb-1"><?= $item['description']?>
                                                        </div>
                                                        <a href="<?= Url::to(['category/index', 'slug' => $item['cat_slug']]) ?>"
                                                        class="name-cat"><?= $item['cat_name'] ?></a>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php endif;endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-lg-7 home-cus-col7">
                            <!-- start thoi su-->
                            <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidget1-1_4", "slug" => "thoi-su", "name" => "Thời Sự", "cate" => 5, "item" => 7]) ?>
                            <!-- end thoi su-->

                            <!-- start van de su kien-->
                            <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidget2-1_4", "slug" => "van-de-su-kien", "name" => "Vấn đề sự kiện", "cate" => 4, "item" => 5]) ?>
                            <!-- end van de su kien-->

                            <!-- start nong nghiep nong thon-->
                            <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidget1-1_4", "slug" => "nong-nghiep-nong-thon", "name" => "Nông nghiệp nông thôn", "cate" => 6, "item" => 7]) ?>
                            <!-- end nong nghiep nong thon-->
                            <div class="row mb-10 gutter-10">
                                <div class="col-md-8 mb-10">
                                    <div class="pt-15">
                                        <a href="" class="d-block"><img src="../images/bn2.png" alt="" class="w-100"></a>
                                    </div>
                                </div>
                                <div class="col-md-4 ispc">
                                    <h2 class="t-14 cl-00baff">Tin tài trợ</h2>
                                    <div class="border-top-e4e4e5 pt-10">
                                        <div class="mb-15 d-flex d-md-block">
                                            <a href="" class="d-block mb-2 mr-2 mr-md-0"><img src="../images/article_thumb_1.png"
                                                                                alt="" width="146"></a>
                                            <h3 class="t-14 f-roboto-b flex-md-1 t-16-mb"><a href="" class="link_unstyle">Danh sách các nha tuyển
                                                    dụng</a></h3>
                                        </div>
                                        <div class="mb-15 d-flex d-md-block">
                                            <a href="" class="d-block mb-2 mr-2 mr-md-0"><img src="../images/article_thumb_1.png"
                                                                                alt="" width="146"></a>
                                            <h3 class="t-14 f-roboto-b flex-md-1 t-16-mb"><a href="" class="link_unstyle">Cơ hội chữa ung thư gan, dạ
                                                    dày, thực quản</a></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row gutter-10">
                        <div class="col-lg-5 home-cus-col5">
                            <!-- start kinh te-->
                            <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidget2-0_4", "slug" => "kinh-te", "name" => "Kinh tế", "cate" => 8, "item" => 6]) ?>
                            <!-- end kinh te-->
               
                        </div>
                        <div class="col-lg-7 home-cus-col7">
                            <!-- start nhip song-->
                            <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidget1-1_4", "slug" => "nhip-song", "name" => "Nhịp sống", "cate" => 15, "item" => 7]) ?>
                            <!-- end nhip song-->
             
                        </div>
                    </div>

                    <div class="row gutter-10">
                        <div class="col-lg-5 home-cus-col5">
                            <!-- start van hoa-->
                            <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidget1-1_2", "slug" => "van-hoa", "name" => "Văn hóa", "cate" => 17, "item" => 5]) ?>
                            <!-- end van hoa-->
                        </div>
                        <div class="col-lg-7 home-cus-col7">
                            <!-- start tu van luat-->
                            <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidget1-1_4", "slug" => "tu-van-luat", "name" => "Tư vấn luật", "cate" => 9, "item" => 7]) ?>
                            <!-- end tu van luat-->
                        </div>
                    </div>
                    <div class="row gutter-10">
                        <div class="col-lg-5 home-cus-col5">
                              <!-- start tieu dung-->
                              <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidget2-0_4", "slug" => "tieu-dung", "name" => "Tiêu dùng", "cate" => 10, "item" => 2]) ?>
                                <!-- end tieu dung-->
                        </div>
                        <div class="col-lg-7 home-cus-col7">
                             <!-- start chinh sach moi-->
                             <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidget2-1_4", "slug" => "chinh-sach-moi", "name" => "Chính sách mới", "cate" => 11, "item" => 5]) ?>
                            <!-- end chinh sach moi-->
                        </div>
                    </div>
                    <div class="row gutter-10">
                        <div class="col-lg-5 home-cus-col5">
                            <!-- start lien ket-->
                            <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidget1-0_4", "slug" => "lien-ket", "name" => "Liên kết", "cate" => 16, "item" => 5]) ?>
                                <!-- end lien ket-->
                        </div>
                        <div class="col-lg-7 home-cus-col7">
                        <div class="row mb-10 gutter-10">
                                <div class="col-md-8 mb-10">
                                    <div class="pt-15">
                                        <a href="" class="d-block"><img src="../images/bn2.png" alt="" class="w-100"></a>
                                    </div>
                                </div>
                                <div class="col-md-4 ispc">
                                    <h2 class="t-14 cl-00baff">Tin tài trợ</h2>
                                    <div class="border-top-e4e4e5 pt-10">
                                        <div class="mb-15 d-flex d-md-block">
                                            <a href="" class="d-block mb-2 mr-2 mr-md-0"><img src="../images/article_thumb_1.png"
                                                                                              alt="" width="146"></a>
                                            <h3 class="t-14 f-roboto-b flex-md-1 t-16-mb"><a href="" class="link_unstyle">Danh sách các nha tuyển
                                                    dụng</a></h3>
                                        </div>
                                        <div class="mb-15 d-flex d-md-block">
                                            <a href="" class="d-block mb-2 mr-2 mr-md-0"><img src="../images/article_thumb_1.png"
                                                                                              alt="" width="146"></a>
                                            <h3 class="t-14 f-roboto-b flex-md-1 t-16-mb"><a href="" class="link_unstyle">Cơ hội chữa ung thư gan, dạ
                                                    dày, thực quản</a></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3">
                    <div class="p-10 bg-e1e1e1 mb-30">
                        <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidgetOther", "slug" => "tap-chi-in", "name" => "Tạp chí in", "cate" => 71, "item" => 12]) ?>
                        <?php if ($readmonth): ?>
                        <div class="feedback bg-fff">
                            <h2 class="t-20 f-roboto-b feedback_title">Ý kiến bạn đọc</h2>
                            <ul class="list-post">
                                <?php foreach ($readmonth as $item): ?>
                                <li>
                                    <h3 class="f-roboto-b t-15 mb-6"><a href="<?= Url::to(['news/detail', 'slug1' => $item['slug']]) ?>" class="link_unstyle"><?= $item['title']?></a></h3>
                                    <div class="max-line max-line-3">
                                        <?= $item['description']?>
                                    </div>
                                </li>
                              <?php endforeach;?>
                            </ul>
                        </div>
                        <?php endif; ?>
                    </div>
                    
                    <?php if($newshot):?>
                    <div class="articles-last bg-fff mb-15 ispc">
                        <h2 class="t-20 f-roboto-b articles-last_title">Có thể bạn quan tâm</h2>
                        <ul class="list-post-global list-post p-10">
                            <?php foreach($newshot as $k=>$item): ?>
                            <li>
                                <h3 class=" t-15"><a href="<?= Url::to(['news/detail', 'slug1' => $item['slug']]) ?>" class="link_unstyle"><?= $item['title'] ?> 
                                <?php if ($news[1]['type_news'] == 1): ?><i class="fa fa-camera cl-999999"
                                                                             aria-hidden="true"></i><?php endif; ?>    
                                </a></h3>
                            </li>
                           <?php endforeach;?>
                        </ul>
                    </div>
                    <?php endif;?>
                    <div class="p-1 border ads mb-10"><?= AdsWidget::widget(["width"=>"300","height"=>"250", "id" => 5]) ?></div>
                    <div class="p-1 ispc"><?= AdsWidget::widget(["width"=>"300","height"=>"250", "id" => 6]) ?></div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
