<?php

use app\components\widgets\AdsWidget;
use app\components\widgets\ViewCateHomeWidget;
use backend\models\News;
use frontend\services\BaseService;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

// var_dump($catst);die();

$this->title = "Doanh Nghiệp và thương hiệu ";
$this->registerMetaTag(['property' => 'og:description', 'content' => \Yii::$app->params['home_description']]);
$this->registerMetaTag(['property' => 'og:title', 'content' => \Yii::$app->params['home_title']]);
$this->registerMetaTag(['name' => 'keywords', 'content' => \Yii::$app->params['home_keyword']]);
$this->registerMetaTag(['property' => 'og:locale', 'content' => \Yii::$app->params['home_locale']]);
$this->registerMetaTag(['property' => 'og:type', 'content' => \Yii::$app->params['home_type']]);
$this->registerMetaTag(['property' => 'og:url', 'content' => \Yii::$app->params['baseUrl']]);
$this->registerMetaTag(['property' => 'og:site_name', 'content' => \Yii::$app->params['home_sitename']]);
?>
<div class="container mb-30">
    <div class="row gutter-10">
        <div class="col-lg-9">
            <div class="d-flex align-items-center mb-15 cat-post">
                <h2 class="t-20 d-line-block text-uppercase mr-10 f-roboto-b title">Tìm kiếm</h2>
                : <?php echo $_GET['keywords']; ?>
            </div>
            <div class="row gutter-10 mb-30">
                <div class="col-md-9">
                    <?php if (isset($searchnew[0])): ?>
                        <ul class="list-post-cat-bottom">
                            <?php foreach ($searchnew as $k => $value): ?>
                                <li>
                                    <h3 class="t-18 f-roboto-b mb-10">
                                        <a href="<?php echo Url::to(['/news/detail', 'slug1' => $value->slug]); ?>"
                                           class="link_unstyle"><?= BaseService::SplitText($value->title, 90) ?></a>
                                    </h3>
                                    <div class="row gutter-10">
                                        <div class="col-lg-4">
                                            <a href="<?php echo Url::to(['/news/detail', 'slug1' => $value->slug]); ?>"
                                               class="d-block mb-1">
                                                <img src="<?= \Yii::$app->params['mediaUrl'] . $value->images ?>"
                                                     class="w-100" alt="<?php echo $value->title ?>"/>
                                            </a>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="d-flex align-items-center mb-6">
                                                <i class="fa fa-clock-o mr-1"
                                                   aria-hidden="true"></i> <?= $value->created_at; ?>
                                            </div>
                                            <div class="max-line max-line-3">
                                                <?= $value->description; ?>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
                <div class="col-md-3">
                    <?= ViewCateHomeWidget::widget(["style" => "CateDetailWidget1", "slug" => "doanh-nghiep-doanh-nhan", "name" => "Doanh nghiệp", "cate" => 62, "item" => 4]) ?>
                    <?= ViewCateHomeWidget::widget(["style" => "CateDetailWidget1", "slug" => "thuong-hieu", "name" => "Thương hiệu", "cate" => 65, "item" => 4]) ?>
                    <?= ViewCateHomeWidget::widget(["style" => "CateDetailWidget1", "slug" => "tai-chinh", "name" => "Bí quyết làm giàu", "cate" => 64, "item" => 4]) ?>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="mb-15">
                <div class="p-1 border mb-2">
                    <?= AdsWidget::widget(["width" => "300", "height" => "250", "id" => 11]) ?>
                </div>
                <div class="p-1 border mb-2">
                    <?= AdsWidget::widget(["width" => "300", "height" => "250", "id" => 12]) ?>
                </div>
            </div>
            <?php
            $docnhieu1 = new News;
            if ($docnhieu1->getReadweek()):
                ?>
                <div class="list-post-detail_2 mb-10">
                    <div class="box-article-cat mb-15 border">
                        <div class="box-article-cat_head pt-5px pb-5px">
                            <h2 class="f-roboto-b t-20 title">Được quan tâm</h2>
                        </div>
                        <ul class="list-post">
                            <?php foreach ($docnhieu1->getReadweek() as $dn1): ?>
                                <li>
                                    <div class="media">
                                        <a href="<?php echo Url::to(['/news/detail', 'slug1' => $dn1->slug]); ?>"
                                           class="d-block">
                                            <?= Html::img(\Yii::$app->params['mediaUrl'] . $dn1->images, ['alt' => $dn1->slug, 'class' => 'mr-5px', 'width' => 100]) ?>
                                        </a>
                                        <div class="media-body">
                                            <h3 class="t-14 f-roboto-b mb-1 max-line max-line-3"><a
                                                        href="<?php echo Url::to(['/news/detail', 'slug1' => $dn1->slug]); ?>"
                                                        class="link_unstyle"><?= BaseService::SplitText($dn1->title, 90) ?></a>
                                            </h3>
                                            <div class="cl-737373 t-11 d-flex align-items-center">
                                                <img src="../images/icon-clock.png" alt=""
                                                     class="mr-1"><span><?php echo $dn1->created_at ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
            <?= ViewCateHomeWidget::widget(["style" => "CateDetailWidget2", "slug" => "khuyen-nong", "name" => "Khuyến nông", "cate" => 72, "item" => 4]) ?>
            <?php if (isset($newslast[0])): ?>
                <div class="list-post-detail_2 mb-15">
                    <div class="box-article-cat border">
                        <div class="box-article-cat_head pt-5px pb-5px">
                            <h2 class="f-roboto-b t-20 title">Tin tức mới nhất</h2>
                        </div>
                        <ul class="list-post-global pt-10 list-post-last">
                            <?php foreach ($newslast as $item): ?>
                                <li><a href="<?= Url::to(['news/detail', 'slug1' => $item['slug']]) ?>"
                                       class="link_unstyle f-roboto-b"><?= $item['title'] ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="d-flex justify-content-center mb-30">
    <nav aria-label="Page navigation example">
        <?= LinkPager::widget(['pagination' => $pages,]); ?>
    </nav>
</div>
<div class="container mb-30">
    <div class="border-top-e4e4e5 text-center pt-2">
        <?= AdsWidget::widget(["width" => "728", "height" => "90", "id" => 9]) ?>
    </div>
</div>