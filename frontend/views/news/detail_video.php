<?php

use app\components\widgets\AdsWidget;
use app\components\widgets\ViewCateHomeWidget;
use backend\models\News;
use fronend\services\BaseService;
use backend\models\Comment;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php
$this->title = "Doanh Nghiệp và thương hiệu";
$this->registerMetaTag(['property' => 'og:description', 'content' => "$video->description"]);
$this->registerMetaTag(['name' => 'keywords', 'content' => "$video->keywords"]);
$this->registerMetaTag(['property' => 'og:title', 'content' => "$video->title"]);
$this->registerMetaTag(['property' => 'og:image', 'content' => "http://media.hellomedia.vn/$video->images"]);
$this->registerMetaTag(['property' => 'og:url', 'content' => "http://doanhnghiepthuonghieu.vn/$video->slug" . '.html']);
$this->registerMetaTag(['property' => 'og:locale', 'content' => "vi_VN"]);
$this->registerMetaTag(['property' => 'og:site_name', 'content' => "doanhnghiepthuonghieu"]);
switch ($video->type_videos) {
    case 1:
        $this->registerMetaTag(['property' => 'og: type', 'content' => "https://www.youtube.com/embed/$video->link_video"]);
        break;
    case 2:
        $this->registerMetaTag(['property' => 'og: type', 'content' => "http://doanhnghiepthuonghieu.vn/$video->link_video"]);
        break;
}
?>
<div class="container mb-15">
    <div class="row gutter-10">
        <div class="col-lg-9">
            <div class="d-flex align-items-center mb-15 cat-post">
                <h2 class="t-20 d-line-block text-uppercase f-roboto-b title">Video</h2>
            </div>
            <h1 class="t-25 f-roboto-b cl-083f59 mb-10"><?php echo $video->title; ?></h1>
            <div class="d-flex align-items-center justify-content-between mb-15">
                <div class="info-author cl-737373">
                    <img src="../images/icon-user.png" alt="" class="mr-1"><span
                            class="mr-2"><?php echo $video->nameuser->pseudonym ?></span>
                    <img src="../images/icon-clock.png" alt="" class="mr-1"><span><?php echo $video->created_at; ?> GMT+7</span>
                </div>
            </div>
            <div class="des f-roboto-b mb-10">
                <?php echo $video->description ?>
            </div>
            <div class="mb-30">
                <div class="content overflow-hidden">
                    <?= $video->content ?>

                    <?php if ($video->link_video != "") { ?>
                        <a href="" class="icon-alig">


                            <?php if ($video->type_videos == 1): ?>

                                <?php
                                $link = str_replace('https://www.youtube.com/watch?v=', '', $video->link_video);

                                ?>
                                <iframe style="width: 810px;height: 456px;"
                                        src="<?= 'https://www.youtube.com/embed/' . $link ?>" frameborder="0"
                                        allowfullscreen></iframe>

                            <?php else: ?>
                                <embed style="width: 810px;height: 456px;"
                                       src="<?= \Yii::$app->params['mediaUrl'] . $video->link_video ?>">
                            <?php endif; ?>

                        </a>
                    <?php } ?>
                </div>
                <div class="text-right mb-20"><img src="../images/icon-penci.png" alt=""
                                                   class="d-inline-block mr-1"><span
                            class="f-roboto-b"><?php echo $video->nameuser->pseudonym ?></span>
                </div>
                <div class="wrap-tags mb-15">
                    <img src="../images/icon-tag.png" alt="" class="d-inline-block mr-2"><span class="f-roboto-b">Từ khóa:</span>
                    <ul class="list-tag">
                        <?php if ($video->keywordsid): foreach ($video->keywordsid as $keys): if ($keys->keywordname->tag_name): ?>
                            <li>
                                <?php
                                $slug = new News;
                                $slug = $slug->makeAlias($keys->keywordname->tag_name);
                                ?>
                                <a href="<?php echo Url::to(['/keyword/index', 'id' => $keys->keywords_id, 'slug' => $slug]) ?>"><?php echo $keys->keywordname->tag_name ?></a>
                            </li>
                        <?php endif;endforeach;endif; ?>
                    </ul>
                </div>
                <div class="comment">
                    <div class="d-flex justify-content-between align-items-end mb-1">
                        <h2 class="f-roboto-b t-20">Bình luận</h2>
                    </div>
                    <?php if($video->show_comment==1){ ?>
                        <!-- commnent -->

                        <?php
                        $form = ActiveForm::begin([
                            'id'=>'comment_news_parent',
                            'method'=>'post',
                            'action'=>Url::to(['news/comment'])
                        ]);
                        ?>
                        <!--    start form comment parent -->
                        <div class="row commentnews">

                            <?= $form->field($comment, 'news_id')->hiddenInput(['class'=>'input-text','value'=>$video->id])->label(false) ?>

                            <div class="col-md-12" style="background: #EEEEEE;padding: 10px;">
                                <?= $form->field($comment, 'messenger')->textarea(['class'=>' form-control commentparentmes','placeholder'=>'Nhập nội dung ...','id'=>'show_mes'])->label(false) ?>


                                <a class="btn btn-primary submitphone "  id="show_modal_parents" >Gửi bình luận</a>


                            </div>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12" >


                                <div class="col-md-12" style="margin-bottom: 20px;margin-top:40px;">
                                    <span id="spanCommentCount" class="comment-count"><?php echo $video->countcomment ?> bình luận</span>
                                </div>

                            </div>
                            <!--  modal comment parent -->

                            <div class="modal fade" id="modal-comment-show">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title text-center">Nhập thông tin </h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        </div>
                                        <div class="modal-body">

                                            <?= $form->field($comment, 'name')->textInput(['class'=>'input-text form-control commentparentmes','placeholder'=>'Nhập họ tên ...','id'=>'nameparent']) ?>

                                            <?= $form->field($comment, 'email')->textInput(['class'=>'input-text form-control commentparentmes','placeholder'=>'Nhập nội email...','id'=>'emailparent']) ?>
                                        </div>
                                        <div class="modal-footer ">
                                            <?= Html::submitButton('Hoàn thành', ['class' =>'btn btn-primary input_cmt','id'=>'comment_parent']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <hr>

                        </div>
                        <!--  end form comment -->
                        <?php ActiveForm::end(); ?>
                        <!--  start list comment parent and child -->
                        <div class="list-comment" >
                            <?php if($video->commentid):foreach($video->commentid as $key): ?>
                                <ul class="comments" data-type="list-commentstream" data-parentid="0" data-template="tplListCommentParent" style="margin-bottom: 20px;">
                                    <li id="comment-d5797e20-afb5-11e7-8ad7-7319761b1588" data-type="comment-item" data-isprocessed="1" data-isparent="1" data-id="d5797e20-afb5-11e7-8ad7-7319761b1588" data-parentid="null" data-liked="0" >

                                        <img class="cmt-avatar avatarvideo" data-type="user-avatar" src="images/no-avatar.png" style="width: 38px !important;">

                                        <div class=" cmt-content"  >

                                            <a class="cmt-author"><?php echo $key->name ?><span class="date">  <?php echo $key->created_at ?></span></a> <?php echo $key->messenger ?>

                                        </div>
                                        <div class="action clearfix" style="margin-left: 48px" >
                                            <div id="load-<?php echo $key->id; ?>">

                                                <?php if(isset(Yii::$app->session['like'.$key->id] )): ?>
                                                    <?php echo Html::a('Bỏ thích',['/news/removelike','comment_id'=>$key->id],['class'=>' remove-to-like','style'=>'margin-left:10px','data-id' => $key->id]) ?>

                                                <?php else: ?>
                                                    <?php echo Html::a('Thích',['/news/addlike','comment_id'=>$key->id],['class'=>' add-to-like','style'=>'margin-left:10px','data-id' => $key->id]) ?>
                                                <?php endif; ?>


                                                <a href="javascript:;" style="margin-left: 10px;margin-right: 10px;" data-toggle="collapse" data-target="#demo_dm_<?php echo $key->id ?>" class="act-item"> Trả lời

                                                </a>
                                                <span><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>   <?php echo $key->like_news ?></span>
                                            </div>

                                        </div>

                                </ul>

                                <?php $data = Comment::find()->where(['parent'=>$key->id])->all(); ?>
                                <?php if($data): foreach($data as $datas): ?>
                                    <!-- start list comment child -->
                                    <div class="list-comment-child " style="margin-left:30px;">
                                        <ul class="comments" data-type="list-commentstream" data-parentid="0" data-template="tplListCommentParent" style="margin-bottom: 20px;" >
                                            <li id="comment-d5797e20-afb5-11e7-8ad7-7319761b1588" data-type="comment-item" data-isprocessed="1" data-isparent="1" data-id="d5797e20-afb5-11e7-8ad7-7319761b1588" data-parentid="null" data-liked="0">

                                                <img class="cmt-avatar" data-type="user-avatar" src="images/no-avatar.png" style="width: 38px !important;" >

                                                <div class=" cmt-content">

                                                    <a class="cmt-author"><?php echo $datas->name ?><span class="date">  <?php echo $datas->created_at ?></span></a><?php echo $datas->messenger ?>

                                                </div>
                                                <div id="load-child-<?php echo $datas->id; ?>">
                                                    <?php if(isset(Yii::$app->session['like'.$datas->id] )): ?>
                                                        <?php echo Html::a('Bỏ thích',['/news/removelike','comment_id'=>$datas->id],['class'=>' remove-to-like-child','style'=>'margin-left:50px;','data-id' => $datas->id]) ?>

                                                    <?php else: ?>
                                                        <?php echo Html::a('Thích',['/news/addlike','comment_id'=>$datas->id],['class'=>' add-to-like-child','style'=>'margin-left:50px;','data-id' => $datas->id]) ?>
                                                    <?php endif; ?>
                                                    <span style="margin-left: 7px"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>  <?php echo $datas->like_news ?></span>
                                                </div>

                                        </ul>
                                    </div>
                                <?php endforeach;endif; ?>


                                <!-- endcomment -->
                                <!-- form rep comment -->
                                <div id="demo_dm_<?php echo $key->id ?>" class="collapse">
                                    <?php $form = ActiveForm::begin([
                                        'id'=>"comment_news_child",
                                        'method'=> 'post',
                                        'action'=> Url::to(['news/commentparent'])
                                    ]); ?>

                                    <div class="col-md-12">
                                        <?= $form->field($comment, 'news_id')->hiddenInput(['class'=>'input-text','value'=>$video->id])->label(false) ?>
                                        <?= $form->field($comment, 'parent')->hiddenInput(['class'=>'input-text','value'=>$key->id])->label(false) ?>

                                    </div>

                                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 comment_child" style="background: #EEEEEE;padding: 10px;" >
                                        <?= $form->field($comment, 'messenger')->textarea(['class'=>'input-text form-control commentparentmes','placeholder'=>'Nhập nội dung ...','id'=>'show_child_'.$key->id])->label(false) ?>
                                        <a class="btn btn-primary submitphone "  id="show_modal_childs" data-id="<?php echo $key->id ?>">Gửi bình luận</a>

                                    </div>


                                    <div class="modal fade" id="modal_comment_child_<?php echo $key->id  ?>">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title text-center">Nhập thông tin </h4>
                                                </div>
                                                <div class="modal-body">

                                                    <?= $form->field($comment, 'name')->textInput(['class'=>'input-text form-control commentparentmes','placeholder'=>'Nhập họ tên ...','id'=>'namechild_'.$key->id]) ?>

                                                    <?= $form->field($comment, 'email')->textInput(['class'=>'input-text form-control commentparentmes','placeholder'=>'Nhập nội email...','id'=>'emailchild_'.$key->id]) ?>
                                                </div>
                                                <div class="modal-footer ">
                                                    <?= Html::submitButton('Hoàn thành', ['class' =>'btn btn-primary input_cmt cmt_child','id'=>'comment_child','data-id'=>$key->id]) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php ActiveForm::end(); ?>
                                </div>
                            <?php endforeach;endif; ?>

                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="mb-15">
                <div class="p-1 border mb-2">
                    <?= AdsWidget::widget(["width" => "300", "height" => "250", "id" => 15]) ?>
                </div>
                <div class="p-1 border mb-2">
                    <?= AdsWidget::widget(["width" => "300", "height" => "250", "id" => 14]) ?>
                </div>
            </div>
            <?php
            if ($videoweek):
                ?>
                <div class="list-post-detail_2 mb-10">
                    <div class="box-article-cat mb-15 border">
                        <div class="box-article-cat_head pt-5px pb-5px">
                            <h2 class="f-roboto-b t-20 title">Được quan tâm</h2>
                        </div>
                        <ul class="list-post">
                            <?php foreach ($videoweek as $dn1): ?>
                                <li>
                                    <div class="media">
                                        <a href="<?php echo Url::to(['/news/detail', 'slug1' => $dn1->slug]); ?>"
                                           class="d-block">
                                            <?= Html::img(\Yii::$app->params['mediaUrl'] . $dn1->images, ['alt' => $dn1->slug, 'class' => 'mr-5px', 'width' => 100]) ?>
                                        </a>
                                        <div class="media-body">
                                            <h3 class="t-14 f-roboto-b mb-1 max-line max-line-3"><a
                                                        href="<?php echo Url::to(['/news/detail', 'slug1' => $dn1->slug]); ?>"
                                                        class="link_unstyle"><?php echo $dn1->title ?></a></h3>
                                            <div class="cl-737373 t-11 d-flex align-items-center">
                                                <img src="../images/icon-clock.png" alt=""
                                                     class="mr-1"><span><?php echo $dn1->created_at ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php if (isset($videomoi[0])): ?>
    <div class="box-article-cat mb-15">
        <div class="container">
            <div class="box-article-cat_head mb-20">
                <h2 class="f-roboto-b t-20 title">Video mới nhất</h2>
            </div>
            <div class="box-article-cat_content">
                <div class="row gutter-15 mb-6">
                     <?php  for($i=1;$i<=8;$i++){ if(isset($videomoi[$i])){ ?>
                        <div class="col-md-4 col-lg-3 mb-15">
                            <a href="<?= Url::to(['news/detail', 'slug1' => $videomoi[$i]['slug']]) ?>" class="d-block mb-15">
                                <?= Html::img(\Yii::$app->params['mediaUrl'] . $videomoi[$i]['images'], ['alt' => $videomoi[$i]['title'], 'class' => 'w-100', 'width' => 100]) ?>
                            </a>
                            <h3 class="f-roboto-b t-15"><a
                                        href="<?= Url::to(['news/detail', 'slug1' => $videomoi[$i]['slug']]) ?>"
                                        class="link_unstyle"><?= $videomoi[$i]['title'] ?></a></h3>
                        </div>
                     <?php }} ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="container">
    <div class="border-bottom mb-30"></div>
</div>
<div class="modal fade" id="modal_show_ok">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Thông báo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                Bình luận thành công !!!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Đồng ý</button>

            </div>
        </div>
    </div>
</div>