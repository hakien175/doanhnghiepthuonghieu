<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\News;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use app\components\widgets\AdsWidget;
  
$host = 'http://'.$_SERVER['HTTP_HOST'];
$homeUrl = str_replace('/backend/web', '', Yii::$app->urlManager->baseUrl);
// var_dump($catst);die();
?>

<div class="container category-detail">
		<div class="row breadcrumb">
			<ol class="breadcrumb">
				<li><a href="/">Trang chủ</a></li>
				<li class="active"><?php echo $cat->name ?></li>
			</ol>

		</div>

		<div class="row cate-news-top img-100">
			<div class="col-md-9 category-left">
				<div class="row cate-new-top-left">
				<?php if($catss2): ?>
					<div class="col-md-9">
						<div class="row category-detail-top">
							<div class="col-md-12 img-100">
								<a href="<?php echo Url::to(['/news/detail','slug1'=>$catss2[0]->slug]); ?>">
								
									 <img src="<?= \Yii::$app->params['mediaUrl'].$catss2[0]->images ?>" width="628px" height="393px"  alt="<?php echo $catss2[0]->title ?>"/>
								</a>
								<p>
									<a href="<?php echo Url::to(['/news/detail','slug1'=>$catss2[0]->slug]); ?>"><?php echo $catss2[0]->title ?></a>
									
								</p>

								<i class="glyphicon glyphicon-user"></i><span class="user-news"> <?php echo $catss2[0]->nameuser->username ?></span>
								<p>
									<?php echo $catss2[0]->description ?>
								</p>
							</div>
						</div>
					</div>
				<?php else: ?>
					<div class="alert">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>Title!</strong> Không có <?php echo $cat->name ?>
					</div>
				<?php endif; ?>


					<div class="col-md-3 ">
						<div class="row content-top-left">
						<?php if ($catss2): unset($catss2[0]); ?>
							
							<?php foreach ($catss2 as $value): ?>
								
							
							<div class="col-md-12 img-100">
								<a href="<?php echo Url::to(['/news/detail','slug1'=>$value->slug]); ?>">
								
									 <img src="<?= \Yii::$app->params['mediaUrl'].$value->images ?>" alt="<?php echo $value->title ?>"/>
								</a>
								<p>
									<a href="<?php echo Url::to(['/news/detail','slug1'=>$value->slug]); ?>">
									<?php echo $value->catchuoi($value->title,250); ?>
										
										<?php echo $value->title ?>
									</a>
								</p>
							</div>


						<?php endforeach ?>
						
						<?php endif; ?>	
						</div>
					</div>


				</div>
			</div>
			<div class="col-md-3 category-right">
				<div class="row">
					<div class="col-md-12 hidden-xs">
						<?= AdsWidget::widget(["width"=>"300","height"=>"250", "id" => 8]) ?>
					</div>
					<div class="col-md-12">
						<div id="box-tien-ich" class="box-widget">
							<div class="box-head">Tiện ích</div>
							<div class="box-body">
								<div class="row">
									<a href="#" class="col-xs-3">
										<i class="fa fa-weather"></i>
										<span>Thời tiết</span>
									</a>
									<a href="#" class="col-xs-3">
										<i class="fa fa-gold"></i>
										<span>Giá vàng</span>
									</a>
									<a href="#" class="col-xs-3">
										<i class="fa fa-bar-chart"></i>
										<span>Chứng khoán</span>
									</a>
									<a href="#" class="col-xs-3">
										<i class="fa fa-calendar-o"></i>
										<span>Lịch xem phim</span>
									</a>
								</div>
								<div class="row">
									<a href="#" class="col-xs-4">
										<i class="fa fa-soccer-ball-o"></i>
										<span>Kết quả bóng đá</span>
									</a>
									<a href="#" class="col-xs-4">
										<i class="fa fa-list-ol"></i>
										<span>Bảng xếp hạng bóng đá</span>
									</a>
									<a href="#" class="col-xs-4">
										<i class="fa fa-calendar"></i>
										<span>Lịch thi đấu bóng đá</span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row cate-qc img-100">
			<div class="col-md-12">
				<div class="qc-center-cate">
						<?= AdsWidget::widget(["width"=>"728","height"=>"90", "id" => 9]) ?>
				</div>
			</div>


		</div>

		<div class="row cate-news-bottom img-100">
			<?php Pjax::begin(); ?>
			<div class="col-md-5 img-100 cate-left">
				<?php if ($newhot): unset($newhot[0],$newhot[1],$newhot[2],$newhot[3]); ?>
					
				<?php foreach ($newhot as $key => $new): ?>
				<div class="row">

					<div class="col-md-12">
						<p>
							<?php echo Html::a($new->title,['/news/detail','slug1'=>$new->slug]); ?>
							
						</p>
						<i class="glyphicon glyphicon-user"></i><span class="user-news"> <?php echo $new->nameuser->username ?></span>
					</div>
					<div class="col-md-5">
						<a href="<?php echo Url::to(['/news/detail','slug1'=>$new->slug]); ?>">
						
							 <img src="<?= \Yii::$app->params['mediaUrl'].$new->images ?>" width="174px" height="119px" alt="<?php echo $new->title ?>"/>
						</a>
					</div>
					<div class="col-md-7">
						<p>
							<?php echo $new->description; ?>
						</p>
					</div>

				</div>
				<hr>
				<?php endforeach ?>
				<?php else: ?>
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>Tin!</strong> không tồn tại ...
					</div>
				<?php endif ?>
				<div class="row text-right">
                    <div class="col-md-12">
    					<?php 
                        echo LinkPager::widget([
                            'pagination' => $pages,
                        ]);
                     ?>
                    </div>
				</div>

			</div>
			<?php Pjax::end(); ?>

			<div class="col-md-7 img-100 cate-right">
				<div class="row">
					<div class="col-md-6 cate-right1">
						<div class="row title-cate">
							<div class="col-md-12">
								<p class="title-cate-right">sự kiện nổi bật</p>
							</div>
						</div>
						<?php if ($eventnews): unset($eventnews[0],$eventnews[1],$eventnews[2],$eventnews[3],$eventnews[4]); ?>
							
						
						
							
						
						<div class="row">
							<?php $n=1; foreach ($eventnews as $key => $noibat): ?>
							<div class="col-md-6">
								<a href="<?php echo Url::to(['/news/detail','slug1'=>$noibat->slug,'newscate'=>$noibat->newnames->slug]); ?>">
								
									<img src="<?= \Yii::$app->params['mediaUrl'].$noibat->images ?>" width="155px" height="101px" alt="<?php echo $noibat->title ?>"/>
								</a>
								<p>
									<?php echo Html::a($noibat->title,['/news/detail','slug1'=>$noibat->slug]); ?>
								
								</p>

							</div>
							<?php 
							if($n == 2) {
							  echo '</div>';
							  echo '<div class="row">';
							}
							if($n == 4) {
							  echo '</div>';
							  echo '<div class="row">';
							}
							if($n == 6) {
							  echo '</div>';
							  echo '<div class="row">';
							}
							if($n == 8) {
							  echo '</div>';
							  echo '<div class="row">';
							}
							
							?>
							<?php $n++; endforeach ?>
						</div>


						
						<?php else: ?>
							<div class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<strong>Tin!</strong> Không tồn tại ...
							</div>
						<?php endif ?>

						<div class="row">
							<div class="panel panel-default panel-mod panel-tab">                            
								<div class="panel-body">
									<div class="row">
										<ul class="nav nav-tabs" role="tablist">
											<li role="presentation" class="active col-md-12">
												<a href="#nhattuan" aria-controls="nhattuan" role="tab" data-toggle="tab">Đọc nhiều nhất tuần</a>
											</li>
										</ul>
									</div>
									
									<?php 
										$docnhieu = new News;

									?>

									<div class="tab-content">
										<div role="tabpanel" class="tab-pane active" id="nhattuan">
											
											<?php foreach ($docnhieu->getReadweek() as $dn): ?>
												
											
											<a class="item" href="<?php echo Url::to(['/news/detail','slug1'=>$dn->slug]); ?>">
										
												<p><?php echo $dn->newnames->name ?></p>
												<p><?php echo $dn->title ?></p>
												<small><?php echo $dn->created_at ?></small>
											</a>

											<?php endforeach ?>

											
										</div>


									</div>


								</div>
							</div>

						</div>

					</div>
					<div class="col-md-6">
						<!-- start thong tin doanh nghiep-->
						<div class="row list-post">
							<div class="col-md-12">
								<div class="panel panel-default panel-dn">
									<div class="panel-heading">
										<span class="panel-title title-doanhnghiep">thông tin doanh nghiệp</span> 
										<a href="#" class="pull-right">Đăng bài PR</a>
										<div class="clearfix"></div>
									</div>
										
										<?php 
											$doanhnghiep = News::find()->where(['cat_id'=>62])->orderBy('id DESC')->limit(3)->all();
											// var_dump($doanhnghiep);die();
										?>

									<div class="panel-body">

										<?php if ($doanhnghiep): ?>
											
										<?php foreach ($doanhnghiep as $key => $dn): ?>
											
										
										<div class="row">
											<div class="col-md-4 img-100  col-xs-4">
												
													<img src="<?= \Yii::$app->params['mediaUrl'].$dn->images ?>" alt="<?php echo $dn->title ?>"/>
												
											</div>
											<div class="col-md-8 col-xs-8">

												<?php echo Html::a($dn->title,['/news/detail','slug1'=>$dn->slug]); ?>
												

											</div>
										</div>
										<hr>

										<?php endforeach ?>
										<?php else: ?>
										<div class="alert alert-danger">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
											<strong>Tin!</strong> không tồn tại ...
										</div>
										<?php endif ?>
										
										
										<?php
											$doanhnghiep1 = News::find()->where(['cat_id'=>62])->orderBy('id DESC')->limit(4)->all();
											
										?>
										
										<?php if ($doanhnghiep1): ?>
											
										
										<div class="row list">
											<div class="col-md-12">
												<ul>
													<?php foreach ($doanhnghiep1 as $key => $dn1): ?>
														
													
													<li>
														<?php echo Html::a($dn1->title,['/news/detail','slug1'=>$dn1->slug]); ?>
															
													</li>
													
													<?php endforeach ?>
												</ul>
											</div>
										</div>
										<?php else: ?>
											<div class="alert alert-danger">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
												<strong>Tin!</strong> không tồn tại ...
											</div>
										<?php endif ?>

									</div>
								</div>
							</div>
						</div>
						<!-- end thong tin doanh nghiep-->
						<div class="row img-100 hidden-xs">
							<div class="col-md-12 qc-left-cate">
								<?= AdsWidget::widget(["width"=>"300","height"=>"600", "id" => 8]) ?>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
