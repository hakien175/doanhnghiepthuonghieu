<?php
use app\components\widgets\ViewCateHomeWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\News;
use yii\widgets\LinkPager;
use app\components\widgets\AdsWidget;
use frontend\services\BaseService;
// var_dump($catst);die();

	$this->title = "Doanh Nghiệp và thương hiệu ";
	$this->registerMetaTag(['property' => 'og:description', 'content' => \Yii::$app->params['home_description']]);
	$this->registerMetaTag(['property' => 'og:title', 'content' => \Yii::$app->params['home_title']]);
	$this->registerMetaTag(['name' => 'keywords', 'content' => \Yii::$app->params['home_keyword']]);
	$this->registerMetaTag(['property' => 'og:locale', 'content' => \Yii::$app->params['home_locale']]);
	$this->registerMetaTag(['property' => 'og:type', 'content' => \Yii::$app->params['home_type']]);
	$this->registerMetaTag(['property' => 'og:url', 'content' => \Yii::$app->params['baseUrl']]);
	$this->registerMetaTag(['property' => 'og:site_name', 'content' => \Yii::$app->params['home_sitename']]);
?>
<div class="container mb-30">
    <div class="row gutter-10">
        <div class="col-lg-9">
            <div class="d-flex align-items-center mb-15 cat-post">
                <h2 class="t-20 d-line-block text-uppercase mr-30 f-roboto-b title"><?= $cat->name ?></h2>
            </div>
            <?php if(isset($cate_new[1]) && !isset($_GET['page'])):?>
            <div class="row pb-3 gutter-10 hot-cate">
                <div class="col-lg-9">
                    <a href="<?php echo Url::to(['/news/detail','slug1'=>$cate_new[0]->slug]); ?>" class="d-block mb-10">
                        <img src="<?= \Yii::$app->params['mediaUrl'].$cate_new[0]->images ?>"  class="w-100"  alt="<?php echo $cate_new[0]->title ?>"/>
                    </a>
                    <h3 class="t-24 mb-2 f-roboto-b"><a href="<?php echo Url::to(['/news/detail','slug1'=>$cate_new[0]->slug]); ?>" class="link_unstyle"><?php echo $list_post[0]->title ?></a></h3>
                    <div class="des max-line max-line-3"><?= $cate_new[0]['description']?></div>
                </div>
                <div class="col-lg-3 border-left">
                    <ul class="list-post-cat-top border-top-mb-cat">
                        <?php foreach ($cate_new as $k=>$value):if($k >0 && $k <5): ?>
                        <li class="list-cate-mb">
                            <a href="<?php echo Url::to(['/news/detail','slug1'=>$value->slug]); ?>" class="d-block mb-10 ismb">
                                <img src="<?= \Yii::$app->params['mediaUrl'].$value->images ?>"  class="w-100"  alt="<?php echo $value->title ?>"/>
                            </a>
                            <h3 class="t-14 f-roboto-b mb-6 t-17-mb">
                                <a href="<?php echo Url::to(['/news/detail','slug1'=>$value->slug]); ?>" class="link_unstyle"><?= BaseService::SplitText($value->title,90); ?></a>
                            </h3>
                            <div class="des max-line max-line-3 ispc"><?= $value->description; ?></div>
                            <div style="clear:both" class="ismb"></div>
                        </li>
                        <?php endif;endforeach;?>
                    </ul>
                </div>
            </div>
            <?php endif;?>
            <div class="border-bottom-183873 mb-20 ispc"></div>
            <div class="row gutter-10 mb-30">
                <div class="col-md-9">
                    <?php if(isset($list_post[0])):?>
                    <ul class="list-post-cat-bottom">
                        <?php foreach ($list_post as $k=>$value):if($k <16): ?>
                        <li class="list-cate-mb-more">
                            <h3 class="t-18 f-roboto-b mb-10">
                                <a href="<?php echo Url::to(['/news/detail','slug1'=>$value->slug]); ?>" class="link_unstyle"><?= BaseService::SplitText($value->title,90)?></a>
                            </h3>
                            <div class="row gutter-10">
                                <div class="col-lg-4">
                                    <a href="<?php echo Url::to(['/news/detail','slug1'=>$value->slug]); ?>" class="d-block mb-1">
                                        <img src="<?= \Yii::$app->params['mediaUrl'].$value->images ?>"  class="w-100"  alt="<?php echo $value->title ?>"/>
                                    </a>
                                </div>
                                <div class="col-lg-8 ispc">
                                    <div class="d-flex align-items-center mb-6">
                                        <i class="fa fa-clock-o mr-1" aria-hidden="true"></i> <?= $value->created_at; ?>
                                    </div>
                                    <div class="max-line max-line-3 ">
                                        <?= $value->description; ?>
                                    </div>
                                </div>
                            </div>
                            <div style="clear:both" class="ismb"></div>
                        </li>
                       <?php endif;endforeach;?>
                    </ul>
                    <?php endif;?>
                    <div class="container d-flex justify-content-center mb-30">
                        <nav aria-label="Page navigation example">
                            <?= LinkPager::widget(['pagination' => $pages, 'maxButtonCount'=>5]); ?>
                        </nav>
                    </div>
                </div>
                <div class="col-md-3 ispc">
                    <?= ViewCateHomeWidget::widget(["style" => "CateDetailWidget1", "slug" => "doanh-nghiep-doanh-nhan", "name" => "Doanh nghiệp", "cate" => 62, "item" => 4]) ?>
                    <?= ViewCateHomeWidget::widget(["style" => "CateDetailWidget1", "slug" => "thuong-hieu", "name" => "Thương hiệu", "cate" => 65, "item" => 4]) ?>
                    <?= ViewCateHomeWidget::widget(["style" => "CateDetailWidget1", "slug" => "tai-chinh", "name" => "Bí quyết làm giàu", "cate" => 64, "item" => 4]) ?>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="mb-15">
                <div class="p-1 border mb-2">
                    <?= AdsWidget::widget(["width"=>"300","height"=>"250", "id" => 11]) ?>
                </div>
                <div class="p-1 border mb-2 ispc">
                    <?= AdsWidget::widget(["width"=>"300","height"=>"250", "id" => 12]) ?>
                </div>
            </div>
            <?php
            $docnhieu1 = new News;
            if ($docnhieu1->getReadweek()):
                ?>
                <div class="list-post-detail_2 mb-10">
                    <div class="box-article-cat mb-15 border">
                        <div class="box-article-cat_head pt-5px pb-5px">
                            <h2 class="f-roboto-b t-20 title">Được quan tâm</h2>
                        </div>
                        <ul class="list-post">
                            <?php foreach ($docnhieu1->getReadweek() as $dn1): ?>
                                <li>
                                    <div class="media">
                                        <a href="<?php echo Url::to(['/news/detail', 'slug1' => $dn1->slug]); ?>"
                                           class="d-block">
                                            <?= Html::img(\Yii::$app->params['mediaUrl'] . $dn1->images, ['alt' => $dn1->slug, 'class' => 'mr-5px', 'width' => 100]) ?>
                                        </a>
                                        <div class="media-body">
                                            <h3 class="t-14 f-roboto-b mb-1"><a
                                                        href="<?php echo Url::to(['/news/detail', 'slug1' => $dn1->slug]); ?>"
                                                        class="link_unstyle"><?= BaseService::SplitText($dn1->title,90); ?></a></h3>
                                            <div class="cl-737373 t-11 d-flex align-items-center">
                                                <img src="../images/icon-clock.png" alt=""
                                                     class="mr-1"><span><?php echo $dn1->created_at ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
            <div class="ismb">
            <?= ViewCateHomeWidget::widget(["style" => "CateDetailWidget2", "slug" => "khuyen-nong", "name" => "Khuyến nông", "cate" => 72, "item" => 4]) ?>
            </div>
            <?php if (isset($newslast[0])): ?>
                <div class="list-post-detail_2 mb-15">
                    <div class="box-article-cat border">
                        <div class="box-article-cat_head pt-5px pb-5px">
                            <h2 class="f-roboto-b t-20 title">Tin tức mới nhất</h2>
                        </div>
                        <ul class="list-post-global pt-10 list-post-last">
                            <?php foreach ($newslast as $item): ?>
                                <li><a href="<?= Url::to(['news/detail', 'slug1' => $item['slug']]) ?>"
                                       class="link_unstyle f-roboto-b"><?= BaseService::SplitText($item['title'],90)?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php if (isset($cate_rand[0])): ?>
<div class="box-article-cat mb-15">
    <div class="container">
        <div class="box-article-cat_content">
            <div class="row gutter-15 mb-6">
                <?php foreach($cate_rand as $item): ?>
                <div class="col-md-3 mb-30">
                    <a href="<?= Url::to(['news/detail', 'slug1' => $item->slug]) ?>" class="d-block mb-15">
                        <img src="<?= \Yii::$app->params['mediaUrl'] . $item->images ?>" alt="<?= $item->title ?>"
                                                          class="w-100">
                    </a>
                    <h3 class="f-roboto-b t-15">
                        <a href="<?= Url::to(['news/detail', 'slug1' => $item->slug]) ?>" class="link_unstyle"><?= BaseService::SplitText($item->title,90) ?></a>
                    </h3>
                </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>
<?php endif;?>

