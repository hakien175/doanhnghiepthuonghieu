<?php

use app\components\widgets\AdsWidget;
use backend\models\Category;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\models\News;
use frontend\services\BaseService;
$hotNews = News::find(['title','slug'])->andWhere(['status'=>8])->andWhere(['public'=>1])->limit(5)->OrderBy('id DESC')->all();
?>
<!--- header desktop-->
<header class="header d-none d-lg-block">
    <div class="header_top">
        <div class="container">
            <ul class="header_top_menu">
                <li><a href=""><i class="fa fa-envelope-o d-inline-block mr-1"></i>Liên hệ tòa soạn</a></li>
                <!-- <li><a href="">english</a></li> -->
            </ul>
            <?php $form = ActiveForm::begin([

                'method' => 'get',
                'action' => Url::to(['search/searchnew']),
                'options' => [
                    'class' => 'header_top_form'
                ]

            ]); ?>
            <input type="text" name="keywords" placeholder="Tìm kiếm"/>
            <button class="fa fa-search btn default btn_search"></button>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <div class="header_logo">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php echo Html::a('<img  src="/images/logo.png" alt="">', ['/site/index'], ['class' => 'd-block logo']); ?>
                </div>
                <div class="col-md-9 pl-5">
                    <?= AdsWidget::widget(["width" => "790", "height" => "100", "id" => 1]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="header_menu menu-desktop">
        <div class="container">
            <nav class="navbar navbar-expand-lg p-0">
                <div class="collapse navbar-collapse" id="navbarContent">
                    <ul class="navbar-nav mr-auto menu">
                        <li class="nav-item">
                            <?php echo Html::a('<i class="fa fa-home t-20"></i>', ['/site/index'], ['class' => 'nav-link text-center f-roboto-b']); ?>
                        </li>
                        <?php
                        $i = 1;
                        $cat = new Category;
                        $cat = $cat->getCategoryByParent(0, 20);

                        if ($cat) {
                            foreach ($cat as $cats) {
                                if ($i > 11) break; ?>
                                <li class="nav-item">
                                <a href="<?php echo Url::to(['/category/index', 'slug' => $cats->slug]); ?>"
                                   class="nav-link f-roboto-b"><?php echo $cats->name ?> </a>
                                <?php
                                $cat1 = $cats->getCategoryByParent($cats->id, 20);
                                if ($cat1) { ?>
                                    <ul class="dropdown-menu">
                                        <?php foreach ($cat1 as $cats1) { ?>
                                            <li>
                                                <?php echo Html::a($cats1->name, ['/category/index', 'slug' => $cats1->slug, 'slugparent' => $cats->slug]); ?>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                    </li>
                                    <?php
                                }
                                $i++;
                            }
                        }
                        ?>
                        <li class="nav-item open-menu-big-destop">
                            <div class="dot-menu">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="menu-big">
                                <div class="row">
                                    <?php
                                    if ($cat) {
                                        foreach ($cat as $cats) { ?>
                                            <div class="col-md-2 mb-3">
                                                <ul class="item-menu">
                                                    <li>
                                                        <a href="<?php echo Url::to(['/category/index', 'slug' => $cats->slug]); ?>"><?php echo $cats->name ?> </a>
                                                        <?php
                                                        $cat1 = $cats->getCategoryByParent($cats->id, 20);
                                                        if ($cat1) { ?>
                                                            <ul class="item-menu_sub">
                                                                <?php foreach ($cat1 as $cats1) { ?>
                                                                    <li class="mb-2"><?php echo Html::a($cats1->name, ['/category/index', 'slug' => $cats1->slug, 'slugparent' => $cats->slug], ['class' => 't-12']); ?></li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php } ?>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="line_bottom">
            <div></div>
            <div></div>
        </div>
    </div>
</header>
<section class="top-content t-12 mb-15 mt-10 d-none d-lg-block">
    <div class="container d-flex align-item-center justify-content-between">
        <div class="left">
            <span class="d-inline-block mr-3 f-roboto-b"><?= BaseService::getDayInWeek();?>, <?= date('d').'/'.date('m').'/'.date('Y')?>, <?= date('H');?>:<?= date('i');?> (GMT+7)</span>
            <div class="d-flex align-items-center news-update">
                <span class="text-uppercase f-roboto-b label">Dòng sự kiện</span>
                <marquee direction="left" scrollamount="4" onmouseover="this.stop();" onmouseout="this.start();" >
                    <?php foreach($hotNews as $item): ?>
                    <span class="content-marquee"><a href="<?= Url::to(['news/detail','slug1'=>$item['slug']]) ?>" class="link_unstyle"><?= $item['title']?></a></span>
                    <?php endforeach;?>
                </marquee>
            </div>
        </div>
        <div class="d-flex align-items-center right">
            <div class="dropdown-weather mr-2">
                <div class="dropdown-toggle weather-current name-city" type="button" id="weather" data-toggle="dropdown"
                     aria-haspopup="true" aria-expanded="false">
                    TP HÀ NỘI
                </div>
                <div class="dropdown-menu" aria-labelledby="weather">
                    <a class="dropdown-item box-city active" href="#" data-city="hn">TP HÀ NỘI</a>
                    <a class="dropdown-item box-city" href="#" data-city="hcm">TP HCM</a>
                </div>
            </div>
            <div class="d-flex align-items-end mr-2">
                <span class="f-roboto-b box-temp active" data-temp="hn"><?= BaseService::getInfoWeather(\Yii::$app->params["infoWeather"],"hn")["low_temp"] ?> - <?= BaseService::getInfoWeather(\Yii::$app->params["infoWeather"],"hn")["high_temp"] ?>°C</span>
                <span class="f-roboto-b box-temp" data-temp="hcm"><?= BaseService::getInfoWeather(\Yii::$app->params["infoWeather"],"hcm")["low_temp"] ?> - <?= BaseService::getInfoWeather(\Yii::$app->params["infoWeather"],"hcm")["high_temp"] ?>°C</span>
            </div>
            <div class="socical">
                <img src="../images/socical.png" alt="">
            </div>
        </div>
    </div>
</section>
<!--- end desktop -->
<!--- header mobile -->
   
<header class="heder-mobile mb-10 d-block d-lg-none">
    <div class="container position-relative">
        <div class="bg-062d3f">
            <div class="row gutter-10">
                <div class="col-5 d-flex align-items-center">
                    <?php echo Html::a('<img  src="/images/logo.png" alt="">', ['/site/index']); ?>
                </div>
                <div class="col-7">
                    <ul class="wrap-button-open-menu h-100">
                        <li><span class="f-roboto-b">Hà Nội<br><?= BaseService::getInfoWeather(\Yii::$app->params["infoWeather"],"hn")["low_temp"] ?> - <?= BaseService::getInfoWeather(\Yii::$app->params["infoWeather"],"hn")["high_temp"] ?>°C</span></li>
                        <li>
                            <button class="button-open-menu">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="wrap-menu-mobile d-block d-lg-none">
    <div class="mb-filter js-mb-filter"></div>
    <div class="wrap-menu-mobile_inner js-menu-mobile">
        <button class="btn btn-sm btn-block btn-info rounded-0 js-mb-menu-close" type="button">
            <i class="fa fa-long-arrow-left"></i>
            <span>Quay lại</span>
        </button>
        <ul class="menu-mobile">
            <li class="p-10">
                <?php $form = ActiveForm::begin([
                    'method' => 'get',
                    'action' => Url::to(['search/searchnew']),
                    'options' => [
                        'class' => 'form-serach-mb'
                    ]
                ]); ?>
                <input type="text" class="form-control" name="keywords" placeholder="Tìm kiếm"/>
                <button><i class="fa fa-search"></i></button>
                <?php ActiveForm::end(); ?>
            </li>
            <?php
            $cat = new Category;
            $cat = $cat->getCategoryByParent(0, 20);
            if ($cat): foreach ($cat as $cats) : ?>
                <li>
                    <a href="<?php echo Url::to(['/category/index', 'slug' => $cats->slug]); ?>"><?= $cats->name ?> </a>
                    <?php  $cat1 = $cats->getCategoryByParent($cats->id, 20);if ($cat1): ?>
                        <span class="open-sub-menu"><i class="fa fa-angle-down"></i></span>
                        <ul class="sub-menu">
                            <?php foreach($cat1 as $cats1): ?>
                            <li>
                                <?php echo Html::a($cats1->name, ['/category/index', 'slug' => $cats1->slug, 'slugparent' => $cats->slug]); ?>
                            </li>
                            <?php endforeach;?>
                        </ul>
                    <?php endif; ?>
                </li>
            <?php endforeach;endif; ?>
        </ul>
    </div>
</div>
<!--- end mobile -->