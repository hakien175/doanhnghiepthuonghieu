<?php
use backend\models\News;
use yii\helpers\Url;
use frontend\services\BaseService;
$hotNews = News::find(['title','slug'])->andWhere(['status'=>8])->andWhere(['public'=>1])->limit(5)->OrderBy('id DESC')->all();
?>
<section class="top-content t-12 mb-15 mt-10">
    <div class="container d-flex align-item-center justify-content-between">
        <div class="left">
            <span class="d-inline-block mr-3 f-roboto-b"><?= BaseService::getDayInWeek();?>, <?= date('d').'/'.date('m').'/'.date('Y')?>, <?= date('H');?>:<?= date('i');?> (GMT+7)</span>
            <div class="d-flex align-items-center news-update">
                <span class="text-uppercase f-roboto-b label">Dòng sự kiện</span>
                <marquee direction="left" scrollamount="4" onmouseover="this.stop();" onmouseout="this.start();" >
                    <?php foreach($hotNews as $item): ?>
                    <span class="content-marquee"><a href="<?= Url::to(['news/detail','slug1'=>$item['slug']]) ?>" class="link_unstyle"><?= $item['title']?></a></span>
                    <?php endforeach;?>
                </marquee>
            </div>
        </div>
        <div class="d-flex align-items-center right">
            <div class="dropdown-weather mr-2">
                <div class="dropdown-toggle weather-current name-city" type="button" id="weather" data-toggle="dropdown"
                     aria-haspopup="true" aria-expanded="false">
                    TP HÀ NỘI
                </div>
                <div class="dropdown-menu" aria-labelledby="weather">
                    <a class="dropdown-item box-city active" href="#" data-city="hn">TP HÀ NỘI</a>
                    <a class="dropdown-item box-city" href="#" data-city="hcm">TP HCM</a>
                </div>
            </div>
            <div class="d-flex align-items-end mr-2">
                <span class="f-roboto-b box-temp active" data-temp="hn"><?= BaseService::getInfoWeather(\Yii::$app->params["infoWeather"],"hn")["low_temp"] ?> - <?= BaseService::getInfoWeather(\Yii::$app->params["infoWeather"],"hn")["high_temp"] ?>°C</span>
                <span class="f-roboto-b box-temp" data-temp="hcm"><?= BaseService::getInfoWeather(\Yii::$app->params["infoWeather"],"hcm")["low_temp"] ?> - <?= BaseService::getInfoWeather(\Yii::$app->params["infoWeather"],"hcm")["high_temp"] ?>°C</span>
            </div>
            <div class="socical">
                <img src="../images/socical.png" alt="">
            </div>
        </div>
    </div>
</section>