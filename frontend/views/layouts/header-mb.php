<?php

use backend\models\Category;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use frontend\services\BaseService;

?>
<header class="heder-mobile mb-10">
    <div class="container position-relative">
        <div class="bg-062d3f">
            <div class="row gutter-10">
                <div class="col-5 d-flex align-items-center">
                    <?php echo Html::a('<img  src="/images/logo.png" alt="">', ['/site/index']); ?>
                </div>
                <div class="col-7">
                    <ul class="wrap-button-open-menu h-100">
                        <li><span class="f-roboto-b">Hà Nội<br><?= BaseService::getInfoWeather(\Yii::$app->params["infoWeather"],"hn")["low_temp"] ?> - <?= BaseService::getInfoWeather(\Yii::$app->params["infoWeather"],"hn")["high_temp"] ?>°C</span></li>
                        <li>
                            <button class="button-open-menu">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="wrap-menu-mobile">
    <div class="mb-filter js-mb-filter"></div>
    <div class="wrap-menu-mobile_inner js-menu-mobile">
        <button class="btn btn-sm btn-block btn-info rounded-0 js-mb-menu-close" type="button">
            <i class="fa fa-long-arrow-left"></i>
            <span>Quay lại</span>
        </button>
        <ul class="menu-mobile">
            <li class="p-10">
                <?php $form = ActiveForm::begin([
                    'method' => 'get',
                    'action' => Url::to(['search/searchnew']),
                    'options' => [
                        'class' => 'form-serach-mb'
                    ]
                ]); ?>
                <input type="text" class="form-control" name="keywords" placeholder="Tìm kiếm"/>
                <button><i class="fa fa-search"></i></button>
                <?php ActiveForm::end(); ?>
            </li>
            <?php
            $cat = new Category;
            $cat = $cat->getCategoryByParent(0, 20);
            if ($cat): foreach ($cat as $cats) : ?>
                <li>
                    <a href="<?php echo Url::to(['/category/index', 'slug' => $cats->slug]); ?>"><?= $cats->name ?> </a>
                    <?php  $cat1 = $cats->getCategoryByParent($cats->id, 20);if ($cat1): ?>
                        <span class="open-sub-menu"><i class="fa fa-angle-down"></i></span>
                        <ul class="sub-menu">
                            <?php foreach($cat1 as $cats1): ?>
                            <li>
                                <?php echo Html::a($cats1->name, ['/category/index', 'slug' => $cats1->slug, 'slugparent' => $cats->slug]); ?>
                            </li>
                            <?php endforeach;?>
                        </ul>
                    <?php endif; ?>
                </li>
            <?php endforeach;endif; ?>
        </ul>
    </div>
</div>


   
  