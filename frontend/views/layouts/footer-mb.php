<?php
use app\components\widgets\AdsWidget;
use backend\models\Category;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<footer class="footer-mb">
    <div class="border-footer_mb"></div>
    <div class="footer-mb_logo">
        <div class="container">
            <div class="d-flex align-items-center justify-content-between">
                <a href="<?= Url::to(['/site/index'])?>" class="d-inline-block logo"><img src="../images/logo.png" alt=""></a>
                <ul class="d-flex align-items-center socical">
                    <li><a href="https://www.facebook.com/doanhnghiepvathuonghieu.com.vn/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="https://twitter.com/intent/tweet?text=<?php echo Yii::$app->params['baseUrl'] ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="https://www.youtube.com/"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="border-footer_mb"></div>
    <div class="container">
        <ul class="d-flex flex-wrap footer-mb_menu">
            <?php
            $cat = new Category;
            $cat = $cat->getCategoryByParent(0, 20);
            if ($cat) :foreach ($cat as $cats) : ?>
            <li><a href="<?=  Url::to(['/category/index', 'slug' => $cats->slug]); ?>"><?= $cats->name ?></a></li>
            <?php endforeach;endif; ?>
        </ul>
    </div>
    <div class="border-footer_mb"></div>
    <div class="container">
        <div class="footer-mb-contact">
            <div class="footer-mb-contact_item">
                <h3 class="f-roboto-b t-20">Phó tổng biên tập phụ trách</h3>
                <div>Nguyễn Nam Thắng</div>
            </div>
            <div class="footer-mb-contact_item">
                <h3 class="f-roboto-b t-20">Trưởng ban điện tử</h3>
                <div>Nguyễn Nam Thắng</div>
            </div>
            <div class="footer-mb-contact_item">
                <h3 class="f-roboto-b t-20">Hợp tác quảng cáo</h3>
                <div class="mb-2">marketingTCTH@gmail.com</div>
                <a href="" class="d-inline-block f-roboto-b t-20">090.468.4321</a>
            </div>
        </div>
    </div>
    <div class="border-footer_mb"></div>
    <div class="container">
        <div class="footer-mb-address">
            <div class="d-flex align-items-start mb-1">
                <div class="mr-1 cl-00baff"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                <div>Tầng 3 - Số 17 ngõ 167 Phố Tây Sơn, Đống Đa, Hà Nội</div>
            </div>
            <div class="d-flex align-items-center mb-1">
                <div class="mr-1 cl-7f7f7f"><i class="fa fa-envelope " aria-hidden="true"></i></div>
                <div>tapchidoanhnghiepvathuonghieu@gmail.com</div>
            </div>
            <div class="d-flex align-items-center mb-1">
                <div class="mr-1"><img src="../images/icon-phone-2.png" alt="" class="d-block w-15"></div>
                <div>024.6657.6928 - 034.333.1111</div>
            </div>
        </div>
    </div>
    <div class="border-footer_mb"></div>
    <div class="container">
        <div class="footer-mb-copyright font-italic">
            <h3 class="t-14 f-roboto-b mb-1">Bản quyền thuộc về Tạp chí Doanh nghiệp và Thương hiệu.</h3>
            <div>Mọi hành động sử dụng nội dung của doanhnghiepthuonghieu.vn phải có sự đồng ý bằng văn bản của Tạp chí Doanh nghiệp và Thương hiệu. Tạp chí Doanh nghiệp và Thương hiệu. điện tử không chịu trách nhiệm nội dung ở những trang ngoài</div>
        </div>
    </div>
</footer>
<div id="back-top"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-155229994-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-155229994-1');
</script>

    