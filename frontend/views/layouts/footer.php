<?php
use app\components\widgets\AdsWidget;
use backend\models\Category;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<!--- begin footer desktop -->
<footer class="footer_desktop d-none d-lg-block">
    <div class="footer_desktop_menu1">
        <div class="container">
            <div class="row">
                <div class="col-md-10 border-right-custom">
                    <div class="list-menu flex-wrap">
                        <?php
                        $cat = new Category;
                        $cat = $cat->getCategoryByParent(0, 20);
                        if ($cat) {
                            foreach ($cat as $cats) { ?>
                                <div class="box-menu">
                                    <div class="box-menu_item mb-20">
                                        <a href="<?php echo Url::to(['/category/index', 'slug' => $cats->slug]); ?>"
                                           class="link_unstyle f-roboto-b text-uppercase d-inline-block mb-2"><?php echo $cats->name ?> </a>
                                        <?php
                                        $cat1 = $cats->getCategoryByParent($cats->id, 20);
                                        if ($cat1) { ?>
                                            <ul class="sub-menu">
                                                <?php foreach ($cat1 as $cats1) { ?>
                                                    <li class="mb-1"><?php echo Html::a($cats1->name, ['/category/index', 'slug' => $cats1->slug, 'slugparent' => $cats->slug], ['class' => 'link_unstyle d-inline-block']); ?></li>
                                                <?php } ?>
                                            </ul>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-2 pl-4">
                    <h2 class="t-18 mb-10">Social & Apps</h2>
                    <ul class="list-socical">
                        <li><a href="https://www.facebook.com/doanhnghiepvathuonghieu.com.vn/" class="link_unstyle"
                               target="_blank"><span class="icon"><i class="fa fa-facebook-square"
                                                                     aria-hidden="true"></i></span>Facebook</a>
                        </li>
                        <li><a href="https://www.youtube.com/" class="link_unstyle" target="_blank"><span
                                        class="icon"><i class="fa fa-youtube-square"
                                                        aria-hidden="true"></i></span>Youtube</a>
                        </li>
                        <li><a href="https://www.linkedin.com/" class="link_unstyle" target="_blank"><span class="icon"><i
                                            class="fa fa-linkedin-square"
                                            aria-hidden="true"></i></span>Linkedin</a>
                        </li>
                        <li><a href="https://twitter.com/intent/tweet?text=<?php echo Yii::$app->params['baseUrl'] ?>"
                               class="link_unstyle" target="_blank"><span class="icon"><i class="fa fa-twitter-square"
                                                                                          aria-hidden="true"></i></span>Twitter</a>
                        </li>
                        <li><a href="http://plus.google.com/share?url=<?php echo Yii::$app->params['baseUrl'] ?>"
                               class="link_unstyle"
                               onclick="window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;"><span
                                        class="icon"><i
                                            class="fa fa-google-plus-square"
                                            aria-hidden="true"></i></i></span>Google+</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_desktop_sapoweb mb-40">
        <div class="container">
            <div class="border-top">
                <div class="mt-10 mb-15">
                    <span class="f-roboto-b">Chùm tin</span>
                    Chính trị, Nông sản Việt, Thời sự Nông nghiệp, Xã hội , Phóng sự, tin 24h, Chuyện lạ, Cây
                    thuốc,
                    Nông nghiệp Việt Nam, Chính trị, Nông sản Việt, Thời sự Nông nghiệp, Xã hội , Phóng sự, tin
                    24h,
                    Chuyện lạ, Cây thuốc, Nông nghiệp Việt Nam, Chính trị, Nông sản Việt, Thời sự Nông nghiệp,
                    Xã hội ,
                    Phóng sự, tin 24h, Chuyện lạ, Cây thuốc, Nông nghiệp Việt Nam, Chính trị, Nông sản Việt,
                    Thời sự
                    Nông nghiệp, Xã hội , Phóng sự, tin 24h, Chuyện lạ, Cây thuốc, Nông nghiệp Việt Nam,
                </div>
                <div class="text-center">
                    <?= AdsWidget::widget(["width"=>"728","height"=>"90", "id" => 7]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_desktop_menu2">
        <div class="container">
            <ul class="menu">
                <?php
                if ($cat) {
                    foreach ($cat as $cats) { ?>
                        <li>
                            <a href="<?php echo Url::to(['/category/index', 'slug' => $cats->slug]); ?>"><?php echo $cats->name ?> </a>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="footer_desktop_address">
            <div class="row align-items-center">
                <div class="col-md-3">
                    <?php echo Html::a('<img  src="/images/logo-footer.png" alt="">', ['/site/index'], ['class' => 'd-block']); ?>
                </div>
                <div class="col-md-9">
                    <h2 class="t-14 f-roboto-b mb-8">TẠP CHÍ DOANH NGHIỆP VÀ THƯƠNG HIỆU</h2>
                    <div class="d-flex align-items-center"><i class="fa fa-map-marker mr-1 t-18 cl-00baff"
                                                              aria-hidden="true"></i>Tòa soạn trị sự: Tầng 3 -
                        Số 17 ngõ
                        167 Phố Tây Sơn, Đống Đa, Hà Nội
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="d-flex align-items-center mr-3"><i
                                    class="fa fa-envelope cl-7f7f7f mr-1"></i>tapchidoanhnghiepvathuonghieu@gmail.com
                        </div>
                        <div class="d-flex align-item-center mr-1"><img src="../images/icon-phone.png" alt=""
                                                                        class="d-block w-15"></div>
                        024.6657.6928 | 034.333.1111
                    </div>
                </div>
            </div>
        </div>
        <ul class="footer_desktop_contact">
            <li>
                <div class="wrap-title"><h3 class="t-14 f-roboto-b title">Phó tổng biên tập phụ trách</h3></div>
                <span>Nguyễn nam thắng</span>
            </li>
            <li>
                <div class="wrap-title">
                    <h3 class="t-14 f-roboto-b title">Trưởng ban điện tử</h3>
                </div>
                <span>Trần đô thành</span>
            </li>
            <li>
                <div class="wrap-title">
                    <h3 class="t-14 f-roboto-b title">Giấy phép số :</h3>224/GP-TTĐT
                </div>
                <span>Cục PTTHTTĐT Cấp ngày 30/08/2018</span>
            </li>
            <li>
                <div class="wrap-title">
                    <h3 class="t-14 f-roboto-b title">Hợp tác quảng cáo</h3>
                    <a href="" class="link-quote">Báo giá hợp tác quảng cáo</a>
                </div>
                <div class="d-flex align-center">
                    <div class="d-flex align-items-center"><i class="fa fa-envelope cl-7f7f7f mr-1"></i>marketingTCTH@gmail.com
                    </div>
                    <div class="d-flex align-items-center ml-3">
                        <div class="mr-1"><img src="../images/icon-phone.png" alt="" class="d-block w-15"></div>
                        <a href="" class="f-roboto-b">090.468.4321</a></div>
                </div>
            </li>
        </ul>
    </div>
    <div class="copyright t-12 f-tahoma text-center">
        <div class="container">
            Ghi rõ nguồn “doanhnghiepthuonghieu.vn” khi phát hành lại thông tin từ website này. Tạp chí Doanh
            nghiệp &
            Thương hiệu điện tử không chịu trách nhiệm nội dung ở những trang ngoài
        </div>
    </div>
</footer>
<!--- end footer desktop -->
<!--- footer mobile -->
<footer class="footer-mb d-block d-lg-none">
    <div class="footer-mb_logo">
        <div class="container">
            <div class="d-flex align-items-center justify-content-between">
                <a href="<?= Url::to(['/site/index'])?>" class="d-inline-block logo"><img src="../images/logo.png" alt=""></a>
                <ul class="d-flex align-items-center socical">
                    <li><a href="https://www.facebook.com/doanhnghiepvathuonghieu.com.vn/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="https://twitter.com/intent/tweet?text=<?php echo Yii::$app->params['baseUrl'] ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="https://www.youtube.com/"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="border-footer_mb"></div>
    <div class="container">
        <ul class="d-flex flex-wrap footer-mb_menu">
            <?php
            $cat = new Category;
            $cat = $cat->getCategoryByParent(0, 20);
            if ($cat) :foreach ($cat as $cats) : ?>
            <li><a href="<?=  Url::to(['/category/index', 'slug' => $cats->slug]); ?>"><?= $cats->name ?></a></li>
            <?php endforeach;endif; ?>
        </ul>
    </div>
    <div class="border-footer_mb"></div>
    <div class="container">
        <div class="footer-mb-contact">
            <div class="footer-mb-contact_item">
                <h3 class="f-roboto-b t-20">Phó tổng biên tập phụ trách</h3>
                <div>Nguyễn Nam Thắng</div>
            </div>
            <div class="footer-mb-contact_item">
                <h3 class="f-roboto-b t-20">Trưởng ban điện tử</h3>
                <div>Nguyễn Nam Thắng</div>
            </div>
            <div class="footer-mb-contact_item">
                <h3 class="f-roboto-b t-20">Hợp tác quảng cáo</h3>
                <div class="mb-2">marketingTCTH@gmail.com</div>
                <a href="" class="d-inline-block f-roboto-b t-20">090.468.4321</a>
            </div>
        </div>
    </div>
    <div class="border-footer_mb"></div>
    <div class="container">
        <div class="footer-mb-address">
            <div class="d-flex align-items-start mb-1">
                <div class="mr-1 cl-00baff"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                <div>Tầng 3 - Số 17 ngõ 167 Phố Tây Sơn, Đống Đa, Hà Nội</div>
            </div>
            <div class="d-flex align-items-center mb-1">
                <div class="mr-1 cl-7f7f7f"><i class="fa fa-envelope " aria-hidden="true"></i></div>
                <div>tapchidoanhnghiepvathuonghieu@gmail.com</div>
            </div>
            <div class="d-flex align-items-center mb-1">
                <div class="mr-1"><img src="../images/icon-phone-2.png" alt="" class="d-block w-15"></div>
                <div>024.6657.6928 - 034.333.1111</div>
            </div>
        </div>
    </div>
    <div class="border-footer_mb"></div>
    <div class="container">
        <div class="footer-mb-copyright font-italic">
            <h3 class="t-14 f-roboto-b mb-1">Bản quyền thuộc về Tạp chí Doanh nghiệp và Thương hiệu.</h3>
            <div>Mọi hành động sử dụng nội dung của doanhnghiepthuonghieu.vn phải có sự đồng ý bằng văn bản của Tạp chí Doanh nghiệp và Thương hiệu. Tạp chí Doanh nghiệp và Thương hiệu. điện tử không chịu trách nhiệm nội dung ở những trang ngoài</div>
        </div>
    </div>
</footer>
<!--- end mobile -->
<div id="back-top"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-155229994-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-155229994-1');
</script>

    