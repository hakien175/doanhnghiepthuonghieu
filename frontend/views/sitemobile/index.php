<?php

use app\components\widgets\ViewCateHomeWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\AdsWidget;

$this->title = "Doanh Nghiệp và thương hiệu ";
$this->registerMetaTag(['property' => 'og:description', 'content' => \Yii::$app->params['home_description']]);
$this->registerMetaTag(['property' => 'og:title', 'content' => \Yii::$app->params['home_title']]);
$this->registerMetaTag(['name' => 'keywords', 'content' => \Yii::$app->params['home_keyword']]);
$this->registerMetaTag(['property' => 'og:locale', 'content' => \Yii::$app->params['home_locale']]);
$this->registerMetaTag(['property' => 'og:type', 'content' => \Yii::$app->params['home_type']]);
$this->registerMetaTag(['property' => 'og:url', 'content' => \Yii::$app->params['baseUrl']]);
$this->registerMetaTag(['property' => 'og:site_name', 'content' => \Yii::$app->params['home_sitename']]);
?>

<div class="container">
    <?php if (isset($newslast[0])): ?>
        <div class="mb-30">
            <a href="<?= Url::to(['news/detail', 'slug1' => $newslast[0]['slug']]) ?>" class="d-block mb-15">
                <?= Html::img(\Yii::$app->params['mediaUrl'] . $newslast[0]["images"], ['alt' => $newslast[0]['title']]) ?>
            </a>
            <h3 class="t-20 f-roboto-b mb-10"><a href="<?= Url::to(['news/detail', 'slug1' => $newslast[0]['slug']]) ?>"
                                                 class="link_unstyle"></a><?= $newslast[0]['title'] ?></a></h3>
            <div class="max-line max-line-3 mb-30">
                <?= $newslast[0]['description'] ?>
            </div>
            <?php if (isset($newslast[1])): ?>
                <ul class="list-post-mb border-top">
                    <?php foreach ($newslast as $k => $item): if ($k > 0 && $k < 6): ?>
                        <li>
                            <div class="media">
                                <a href="<?= Url::to(['news/detail', 'slug1' => $item[0]['slug']]) ?>" class="mr-10">
                                    <?= Html::img(\Yii::$app->params['mediaUrl'] . $item["images"], ['alt' => $item['title']]) ?>
                                </a>
                                <div class="media-body">
                                    <h3 class="t-14 f-roboto-b"><a
                                                href="<?= Url::to(['news/detail', 'slug1' => $newslast[0]['slug']]) ?>"
                                                class="link_unstyle"><?= $item['title'] ?></a></h3>
                                </div>
                            </div>
                        </li>
                    <?php endif;endforeach ?>
                </ul>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <div class="p-1 mb-30">
        <?= AdsWidget::widget(["width"=>"300","height"=>"250", "id" => 2]) ?>
    </div>
    <?php if (isset($newslast[6])): ?>
        <ul class="list-post-mb pt-0 mb-30">
            <?php foreach ($newslast as $k => $item): if ($k > 5 && $k < 12): ?>
                <li>
                    <div class="media">
                        <a href="<?= Url::to(['news/detail', 'slug1' => $item[0]['slug']]) ?>" class="mr-10">
                            <?= Html::img(\Yii::$app->params['mediaUrl'] . $item["images"], ['alt' => $item['title']]) ?>
                        </a>
                        <div class="media-body">
                            <h3 class="t-14 f-roboto-b"><a
                                        href="<?= Url::to(['news/detail', 'slug1' => $newslast[0]['slug']]) ?>"
                                        class="link_unstyle"><?= $item['title'] ?></a></h3>
                        </div>
                    </div>
                </li>
            <?php endif;endforeach ?>
        </ul>
    <?php endif; ?>
    <div class="rate mb-30">
        <div class="rate_title mb-15">
            <h2 class="t-25 cl-164069 f-roboto-b">Tỷ giá</h2>
        </div>
        <div class="rate_content d-flex mb-1">
            <div class="rate_content_item p-10">
                <div class="border-bottom-fff text-uppercase pb-10 f-roboto-b d-flex justify-content-between align-items-center">
                    <h3 class="t-20 cl-164069">usd</h3>
                </div>
                <div class="d-flex justify-content-between align-items-end pt-1 pb-1">
                    <span class="f-roboto-b cl-696969">Mua</span>
                    <span class="f-roboto-b t-20">23,160</span>
                </div>
                <div class="d-flex justify-content-between mt-20 align-items-end pt-1 pb-1">
                    <span class="f-roboto-b cl-696969">Mua</span>
                    <span class="f-roboto-b t-20">23,160</span>
                </div>
            </div>
            <div class="rate_content_item p-10">
                <div class="border-bottom-fff text-uppercase pb-10 f-roboto-b d-flex justify-content-between align-items-center">
                    <h3 class="t-20 cl-164069">jpy</h3>
                </div>
                <div class="d-flex justify-content-between align-items-end  pt-1 pb-1">
                    <span class="f-roboto-b cl-696969">Mua</span>
                    <span class="f-roboto-b t-20">23,160</span>
                </div>
                <div class="d-flex justify-content-between mt-20 align-items-end  pt-1 pb-1">
                    <span class="f-roboto-b cl-696969">Mua</span>
                    <span class="f-roboto-b t-20">23,160</span>
                </div>
            </div>
        </div>
        <div class="cl-696969">Đơn vị tính: đồng</div>
    </div>
    <div class="rate mb-30">
        <div class="rate_title mb-15">
            <h2 class="t-25 cl-164069 f-roboto-b">Giá nông sản</h2>
        </div>
        <div class="rate_content d-flex mb-1">
            <div class="rate_content_item p-10">
                <div class="border-bottom-fff text-uppercase pb-10 f-roboto-b d-flex justify-content-between align-items-center">
                    <h3 class="t-20 cl-164069">Gạo</h3>
                    Đ/KG
                </div>
                <div class="d-flex justify-content-between align-items-end pt-1 pb-1">
                    <span class="f-roboto-b cl-696969">Tuần này</span>
                    <span class="f-roboto-b t-20">5.990</span>
                </div>
                <div class="d-flex justify-content-between mt-20 align-items-end pt-1 pb-1">
                    <span class="f-roboto-b cl-696969">Tuần trước</span>
                    <span class="f-roboto-b t-20">5.990</span>
                </div>
            </div>
            <div class="rate_content_item p-10">
                <h3 class="t-20 cl-164069 f-roboto-b text-uppercase pb-10  border-bottom-fff">Cà phê</h3>
                <div class="d-flex justify-content-between align-items-end  pt-1 pb-1">
                    <span class="f-roboto-b cl-696969">Tuần này</span>
                    <span class="f-roboto-b t-20">5.990</span>
                </div>
                <div class="d-flex justify-content-between mt-20 align-items-end  pt-1 pb-1">
                    <span class="f-roboto-b cl-696969">Tuần trước</span>
                    <span class="f-roboto-b t-20">5.990</span>
                </div>
            </div>
        </div>
        <div class="cl-696969">Đơn vị tính: đồng</div>
    </div>
    <?php if (isset($newslast[12])): ?>
        <div class="mb-30">
            <a href="<?= Url::to(['news/detail', 'slug1' => $newslast[12]['slug']]) ?>" class="d-block mb-15">
                <?= Html::img(\Yii::$app->params['mediaUrl'] . $newslast[12]["images"], ['alt' => $newslast[12]['title']]) ?>
            </a>
            <h3 class="t-20 f-roboto-b mb-10"><a
                        href="<?= Url::to(['news/detail', 'slug1' => $newslast[12]['slug']]) ?>"
                        class="link_unstyle"></a><?= $newslast[12]['title'] ?></a></h3>
            <div class="max-line max-line-3 mb-30">
                <?= $newslast[12]['description'] ?>
            </div>
            <ul class="list-post-mb border-top">
                <?php foreach ($newslast as $k => $item): if ($k > 13): ?>
                    <li>
                        <div class="media">
                            <a href="<?= Url::to(['news/detail', 'slug1' => $item[0]['slug']]) ?>" class="mr-10">
                                <?= Html::img(\Yii::$app->params['mediaUrl'] . $item["images"], ['alt' => $item['title']]) ?>
                            </a>
                            <div class="media-body">
                                <h3 class="t-14 f-roboto-b"><a
                                            href="<?= Url::to(['news/detail', 'slug1' => $newslast[0]['slug']]) ?>"
                                            class="link_unstyle"><?= $item['title'] ?></a></h3>
                            </div>
                        </div>
                    </li>
                <?php endif;endforeach ?>
            </ul>
        </div>
    <?php endif; ?>
    <?php if (isset($videos[0])): ?>
        <section class="video-home mb-30 border-0">
            <div class="border-footer_mb"></div>
            <div class="video-home_title border-0">
                <h2 class="f-roboto-b t-14 d-inline-flex align-items-center">
                    <img src="../images/icon-video.png" alt="" class="d-inline-block mr-2">Video
                </h2>
            </div>
            <a href="<?= Url::to(['/news/detail', 'slug1' => $videos[0]['slug']]); ?>" class="d-block mb-15 position-relative">
                <?= Html::img(\Yii::$app->params['mediaUrl'] . $videos[0]["images"], ['alt' => $videos[0]['title']]) ?>
                <img alt="" src="../images/icon-video_2.png" class="icon-video">
            </a>
            <h3 class="t-20 f-roboto-b mb-10">
                <a href="<?= Url::to(['/news/detail', 'slug1' => $videos[0]['slug']]); ?>" class="link_unstyle"><?= $videos[0]['title']?></a>
            </h3>
                <?php if (isset($videos[1])): ?>
                <div class="row border-top gutter-10 pt-10">
                    <?php foreach ($videos as $item): ?>
                    <div class="col-6">
                        <a href="<?= Url::to(['/news/detail', 'slug1' => $item['slug']]); ?>" class="d-block mb-10 position-relative">
                            <?= Html::img(\Yii::$app->params['mediaUrl'] . $item["images"], ['alt' => $item['title']]) ?>
                            <img alt="" src="../images/icon-video_2.png" class="icon-video">
                        </a>
                        <h3 class="t-14 f-roboto-b mb-10"><a href="<?= Url::to(['/news/detail', 'slug1' => $item['slug']]); ?>" class="link_unstyle"><?= $item['title']?></a></h3>
                    </div>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>
        </section>
    <?php endif; ?>
    <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidgetMobile1", "slug" => "thoi-su", "name" => "Thời Sự", "cate" => 5, "item" => 3]) ?>
    <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidgetMobile1", "slug" => "kinh-te", "name" => "Kinh tế", "cate" => 4, "item" => 3]) ?>
    <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidgetMobile1", "slug" => "nong-nghiep-nong-thon", "name" => "Nông nghiệp nông thôn", "cate" => 6, "item" => 3]) ?>
</div>
<?= ViewCateHomeWidget::widget(["style" => "CateHomeWidgetMobile2", "slug" => "tap-chi-in", "name" => "Tạp chí in", "cate" => 71, "item" => 3]) ?>

<div class="container">
    <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidgetMobile1", "slug" => "nhip-song", "name" => "Nhịp sống", "cate" => 15, "item" => 3]) ?>
    <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidgetMobile1", "slug" => "van-hoa", "name" => "Văn hóa", "cate" => 17, "item" => 3]) ?>
    <div class="p-1 mb-30">
        <?= AdsWidget::widget(["width"=>"300","height"=>"250", "id" => 3]) ?>
    </div>
    <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidgetMobile1", "slug" => "tu-van-luat", "name" => "Tư vấn luật", "cate" => 9, "item" => 3]) ?>
    <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidgetMobile1", "slug" => "tieu-dung", "name" => "Tiêu dùng", "cate" => 10, "item" => 3]) ?>
    <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidgetMobile1", "slug" => "chinh-sach-moi", "name" => "Chính sách mới", "cate" => 11, "item" => 3]) ?>
    <?= ViewCateHomeWidget::widget(["style" => "CateHomeWidgetMobile1", "slug" => "lien-ket", "name" => "Liên kết", "cate" => 16, "item" => 3]) ?>
    <div class="p-1 mb-30">
        <?= AdsWidget::widget(["width"=>"300","height"=>"250", "id" => 5]) ?>
    </div>
    <div class="p-1 mb-30">
        <?= AdsWidget::widget(["width"=>"300","height"=>"250", "id" => 6]) ?>
    </div>
    <div class="p-1 mb-30">
        <?= AdsWidget::widget(["width"=>"300","height"=>"250", "id" => 6]) ?>
    </div>
</div>
