<?php
use yii\web\Request;
$request = new Request();
$baseUrl = str_replace('/frontend/web','', $request->baseUrl);
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl'=>$baseUrl,
        ],
        'opengraph' => [
          'class' => 'dragonjet\opengraph\OpenGraph',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ]
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
         'request' => [
           'baseUrl' =>$baseUrl,
        ],
        'urlManager' => [
            'baseUrl'=>$baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
              ''=>'site/index',
              
              'gioi-thieu'=>'site/about',
              
              'dang-nhap'=>'site/login',

              'contact'=>'site/contact',

              'captcha'=>'site/captcha',

              'email-news'=>'site/addemail',
              
              'tim-kiem'=>'search/searchnew',

              '/truyen-hinh-dnth'=>'video/index',


              'comment-news'=>'news/comment',

              'comment-news-parent'=>'news/commentparent',

              'comment-news-addlike'=>'news/addlike',

              'comment-news-removelike'=>'news/removelike',


              'tag/<slug>-<id>/trang-<page:\d+>'=>'keyword/index',

              'tag/<slug>-<id>'=>'keyword/index',
              
              '/<slug:[\w-]+>.html'=>'news/detail',

              '/demo/<slug:[\w-]+>.html'=>'news/detail',


              //'/<alias:[\w-]+>/<slug:[\w-]+>/html'=>'video/detail',

              '/<alias:[\w-]+>/<slug:[\w-]+>.html'=>'news/detail',

              //'/<alias:[\w-]+>/<alias1:[\w-]+>/<slug:[\w-]+>.html'=>'news/detail',

              '/<slug>/trang-<page:\d+>'=>'category/index',

              '<slugparent>/<slug>'=>'category/index',

              

              '<slug>'=>'category/index',

              '<slug1>.html'=>'news/detail',

              


              //'video/<slug>.html'=>'news/detail',

            ],
        ],
    
    ],
    'params' => $params,
];
