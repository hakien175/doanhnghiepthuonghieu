<?php
return [
    'adminEmail' => 'admin@example.com',
    'mediaUrl'=>'http://mediadnth.90phut.vn/',
    'baseUrl'=>'http://doanhnghiepthuonghieu.vn/',
    //tag SEO
    'home_title'=>'Tin nhanh - Đọc báo, tin tức online 24h',
    'home_description'=>'Thông tin nhanh và mới nhất được cập nhật hàng giờ. Tin tức Việt Nam và thế giới về xã hội, kinh doanh, pháp luật, khoa học, công nghệ, sức khoẻ, đời sống, văn hóa, tâm linh, chuyện lạ, tâm sự...',
    'home_keyword'=>'Tin tức, đọc báo, đọc báo online, pháp luật, xã hội, thể thao, bóng đá, âm nhạc, ngôi sao, điện ảnh, phim, vụ án, hình sự, đời sống, sức khỏe, xe máy, xe ô tô, xe độ, kinh tế, kinh doanh, tài chính, chứng khoán',
    'home_image'=>'http://doanhnghiepthuonghieu.vn/images/logocat1.png',
    'home_sitename'=>'doanhnghiepthuonghieu.vn',
    'home_type'=>'website',
    'home_locale'=>'vi_VN',
    'infoWeather' =>
        [
            "hn"=>"1581130",
            "hcm"=>"1566083",
        ],
];
