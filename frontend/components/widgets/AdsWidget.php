<?php
namespace app\components\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Advertisement;


class AdsWidget extends Widget{
	
	public $width;
	public $height;
    public $id;
//	
	public function init(){
		parent::init();		
	}
	
	public function run(){
            
            
            $ads = Advertisement::find()
		    ->where(['id' => $this->id])
		    ->one();
            if($ads){
            	if($ads->active==1){
            	if($ads->type ==0){
            		echo $ads->link_advertisement;
            	}else{
            		$ext = explode('.', $ads->link_advertisement);
            		$ext = $ext[count($ext)-1];	
            		if($ext == 'swf'){
            			echo '<embed src="'.\Yii::$app->params['mediaUrl'].$ads->link_advertisement.'" wmode="direct" id="play_game" quality="high" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer/" width="'.$this->width.'px" height="'.$this->height.'px"  name="preloader" align="middle" allowscriptaccess="always" autoplay="true" allowfullscreen="false" flashvars="adTagUrl=">';
            		}else{
            			echo "<a href='".$ads->link_taget."' class='d-block text-center'><img src='".\Yii::$app->params['mediaUrl'].$ads->link_advertisement."' width='".$this->width."px' height='".$this->height."px' /></a>";
            		}
            		
            	}
              }
            }
            
           
	}
}
?>