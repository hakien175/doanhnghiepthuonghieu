<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\services\BaseService;
?>
<?php $news = $data["news"]; ?>
<div class="box-article-cat mb-15">
    <div class="box-article-cat_head mb-15">
        <h2 class="f-roboto-b t-20 title"><a href="<?php echo Url::to(['/category/index', 'slug' => $slug]); ?>"
                                             class="link_unstyle"><?= $name ?></a></h2>
        <ul class="cat-child">
            <?php
            if ($data["subcate"]): foreach ($data["subcate"] as $row):
                ?>
                <li>
                    <a href="<?= Url::to(['/category/index', 'slug' => $row["slug"], 'slugparent' => $slug]); ?>">
                        <?= $row["name"] ?>
                    </a>
                </li>
            <?php endforeach;endif; ?>
        </ul>
    </div>
    <?php if(isset($news[0])): ?>
    <div class="box-article-cat_content">
        <div class="row gutter-10">
            <div class="col-lg-6 mb-10">
                <a href="<?= Url::to(['/news/detail', 'slug1' => $news[0]['slug']]); ?>" >
                    <?= Html::img(\Yii::$app->params['mediaUrl'] . $news[0]["images"], ['alt' => $news[0]['title'], 'class' => 'w-100']) ?>
                </a>
            </div>
            <div class="col-lg-6 mb-10">
                <h3 class="t-14 f-roboto-b mb-6 t-17-mb"><a href="<?= Url::to(['/news/detail', 'slug1' => $news[0]['slug']]); ?>" class="link_unstyle"><?= $news[0]['title']?></a></h3>
                <a href="<?= Url::to(['category/index','slug'=>$news[0]['cat_slug']])?>" class="name-cat"><?= $news[0]['cat_name'] ?></a>
            </div>
        </div>
        <?php if(isset($news[1])): ?>
        <div class="border-top-e4e4e5 pt-10">
            <div class="row gutter-10">
                <div class="col-md-8 border-right-e4e4e5 mb-10">
                    <div class="media mediavh">
                        <a href="<?= Url::to(['/news/detail', 'slug1' => $news[1]['slug']]); ?>" >
                            <?= Html::img(\Yii::$app->params['mediaUrl'] . $news[1]["images"], ['alt' => $news[1]['title'], 'class' => 'mr-10', 'width' => 100]) ?>
                        </a>
                        <div class="media-body">
                            <h3 class="t-14 f-roboto-b mb-6"><a href="<?= Url::to(['news/detail','slug1'=>$news[1]['slug']])?>" class="link_unstyle"><?= BaseService::SplitText($news[1]['title'],80) ?>
                                    <?php if ($news[1]['type_news'] == 1): ?><i class="fa fa-camera cl-999999"
                                                                             aria-hidden="true"></i><?php endif; ?></a>
                            </h3>
                            <a href="<?= Url::to(['category/index','slug'=>$news[1]['cat_slug']])?>" class="d-block name-cat"><?= $news[1]['cat_name'] ?></a>
                        </div>
                    </div>
                    <div class="media">
                        <a href="<?= Url::to(['/news/detail', 'slug1' => $news[2]['slug']]); ?>" >
                            <?= Html::img(\Yii::$app->params['mediaUrl'] . $news[2]["images"], ['alt' => $news[2]['title'], 'class' => 'mr-10', 'width' => 100]) ?>
                        </a>
                        <div class="media-body">
                            <h3 class="t-14 f-roboto-b mb-6"><a href="<?= Url::to(['news/detail','slug1'=>$news[2]['slug']])?>" class="link_unstyle"><?= BaseService::SplitText($news[2]['title'],80) ?>
                                    <?php if ($news[2]['type_news'] == 1): ?><i class="fa fa-camera cl-999999"
                                                                             aria-hidden="true"></i><?php endif; ?></a>
                            </h3>
                            <a href="<?= Url::to(['category/index','slug'=>$news[1]['cat_slug']])?>" class="d-block name-cat"><?= $news[2]['cat_name'] ?></a>
                        </div>
                    </div>
                </div>
                <?php if(isset($news[3])): ?>
                <div class="col-md-4">
                    <ul class="list-post-global mb-15">
                        <?php foreach($news as $k=>$item) : if($k >2): ?>
                        <li class="mb-6"><a href="<?= Url::to(['news/detail','slug1'=>$item['slug']])?>" class="link_unstyle"><?= BaseService::SplitText($item['title'],65) ?></a></li>
                        <?php endif;endforeach;?>
                    </ul>
                </div>
                <?php endif;?>
            </div>
        </div>
        <?php endif;?>
    </div>
    <?php endif;?>
</div>