<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\services\BaseService;
?>
 <?php
    if (BaseService::detectMobile() == "desktop") {
        $text_length = 80;
    }else{
        $text_length = 150;
    }
?>

<?php $news = $data["news"]; ?>
<div class="box-article-cat mb-15">
    <div class="box-article-cat_head mb-15">
        <h2 class="f-roboto-b t-20 title"><a href="<?php echo Url::to(['/category/index', 'slug' => $slug]); ?>"
                                             class="link_unstyle"><?= $name ?></a></h2>
        <ul class="cat-child">
            <?php
            if ($data["subcate"]): foreach ($data["subcate"] as $row):
                ?>
                <li>
                    <a href="<?= Url::to(['/category/index', 'slug' => $row["slug"], 'slugparent' => $slug]); ?>">
                        <?= $row["name"] ?>
                    </a>
                </li>
            <?php endforeach;endif; ?>
        </ul>
    </div>
    <?php if (isset($news[0])): ?>
    <div class="box-article-cat_content fixblock">
        <div class="row gutter-10">
            <?php foreach ($news as $k => $item) : if ($k < 2): ?>
            <div class="col-md-6 mb-10">
                <div class="media">
                    <a href="<?= URl::to(['news/detail','slug1'=>$item['slug']])?>" class="d-block">
                        <?= Html::img(\Yii::$app->params['mediaUrl'] . $item["images"], ['alt' => $item['title'], 'class' => 'mr-10', 'width' => 100]) ?>
                    </a>
                    <div class="media-body">
                        <h3 class="t-12-dt f-roboto-b mb-10">
                            <a href="<?= URl::to(['news/detail','slug1'=>$item['slug']])?>" class="link_unstyle">
                                <?= BaseService::SplitText($item['title'],$text_length)?>
                                <?php if ($item['type_news'] == 1): ?><i class="fa fa-camera cl-999999"
                                                                            aria-hidden="true"></i><?php endif; ?>
                            </a>
                        </h3>
                    </div>
                </div>
            </div>
            <?php else:break; ?>
            <?php endif;endforeach; ?>
        </div>
        <?php if (isset($news[2])): ?>
        <div class="row gutter-10 mb-15">
            <div class="col-md-6 mb-10">
                <div class="media">
                    <a href="<?= URl::to(['news/detail','slug1'=>$news[2]['slug']])?>" class="d-block">
                        <?= Html::img(\Yii::$app->params['mediaUrl'] . $news[2]["images"], ['alt' => $news[2]['title'], 'class' => 'mr-10', 'width' => 100]) ?>
                    </a>
                    <div class="media-body">
                        <h3 class="t-12-dt f-roboto-b mb-10"><a href="<?= URl::to(['news/detail','slug1'=>$news[2]['slug']])?>" class="link_unstyle"><?= $news[2]['title']?></a></h3>
                    </div>
                </div>
            </div>
            <?php if (isset($news[3])): ?>
            <div class="col-md-6">
                <ul class="list-post-global list-home-mb">
                    <?php foreach($news as $k=>$item): if($k > 2): ?>
                        <li>
                            <a href="<?= URl::to(['news/detail','slug1'=>$item['slug']])?>" class="d-block ismb">
                                <?= Html::img(\Yii::$app->params['mediaUrl'] . $item["images"], ['alt' => $item['title'], 'class' => 'mr-10', 'width' => 100]) ?>
                            </a>
                            <a href="<?= Url::to(['news/detail','slug1'=>$item['slug']])?>" class="link_unstyle f-roboto-b t-12-dt">
                               
                                <?= BaseService::SplitText($item['title'],$text_length) ?>
                                <?php if ($item['type_news'] == 1): ?><i class="fa fa-camera cl-999999"
                                                                            aria-hidden="true"></i><?php endif; ?>
                            </a>
                            <div style="clear:both" class="ismb"></div>
                        </li>
                    <?php endif;endforeach; ?>
                </ul>
            </div>
            <?php endif;?>
        </div>
        <?php endif;?>
    </div>
    <?php endif; ?>
</div>