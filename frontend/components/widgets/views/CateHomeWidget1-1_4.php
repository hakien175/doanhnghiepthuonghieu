<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\services\BaseService;
?>
 <?php
    if (BaseService::detectMobile() == "desktop") {
        $text_length = 80;
    }else{
        $text_length = 150;
    }
?>
<?php $news = $data["news"]; ?>
    <div class="box-article-cat mb-15">
    <div class="box-article-cat_head mb-15">
        <h2 class="f-roboto-b t-20 title"><a href="<?php echo Url::to(['/category/index', 'slug' => $slug]); ?>"
                                             class="link_unstyle"><?= $name ?></a></h2>
        <ul class="cat-child">
            <?php
            if ($data["subcate"]): foreach ($data["subcate"] as $row):
                ?>
                <li>
                    <a href="<?= Url::to(['/category/index', 'slug' => $row["slug"], 'slugparent' => $slug]); ?>">
                        <?= $row["name"] ?>
                    </a>
                </li>
            <?php endforeach;endif; ?>
        </ul>
    </div>
    <div class="box-article-cat_content">
    <?php if (isset($news[0])): ?>
<div class="row gutter-10 mb-10">
    <div class="col-md-6"> <a href="<?= Url::to(['/news/detail', 'slug1' => $news[0]['slug']]); ?>" class="d-block mb-10">
            <?= Html::img(\Yii::$app->params['mediaUrl'] . $news[0]["images"], ['alt' => $news[0]['title'], 'class' => 'mr-10 w-100  img-large']) ?>
        </a></div>
    <div class="col-md-6">
        <h3 class="t-14 f-roboto-b mb-10 t-17-mb"><a href="<?= Url::to(['/news/detail', 'slug1' => $news[0]['slug']]); ?>"
                                             class="link_unstyle"><?= BaseService::SplitText($news[0]['title'],$text_length) ?> <?php if ($news[0]['type_news'] == 1): ?>
                    <i class="fa fa-camera cl-999999" aria-hidden="true"></i><?php endif; ?></a></h3>
        <div class="des max-line max-line-3">
            <?= $news[0]['description'] ?>
        </div>
    </div>
</div>
    <?php if (isset($news[1])): ?>
        <div class="pt-10 mb-15 border-top-e4e4e5 fixblock">
            <div class="row gutter-10">
                <div class="col-md-6 mb-10">
                    <div class="media mediavh">
                        <a href="<?= Url::to(['/news/detail', 'slug1' => $news[1]['slug']]); ?>" class="d-block">
                            <?= Html::img(\Yii::$app->params['mediaUrl'] . $news[1]["images"], ['alt' => $news[1]['title'], 'class' => 'mr-10 ', 'width' => 100]) ?>
                        </a>
                        <div class="media-body">
                            <h3 class="t-12-dt f-roboto-b mb-10">
                                <a href="<?= Url::to(['/news/detail', 'slug1' => $news[1]['slug']]); ?>"
                                   class="link_unstyle"><?= BaseService::SplitText($news[1]['title'],$text_length) ?>
                                    <?php if ($news[1]['type_news'] == 1): ?><i class="fa fa-camera cl-999999"
                                                                                aria-hidden="true"></i><?php endif; ?>
                                </a>
                            </h3>
                        </div>
                    </div>

                    <div class="media">
                        <a href="<?= Url::to(['/news/detail', 'slug1' => $news[2]['slug']]); ?>" class="d-block">
                            <?= Html::img(\Yii::$app->params['mediaUrl'] . $news[2]["images"], ['alt' => $news[2]['title'], 'class' => 'mr-10', 'width' => 100]) ?>
                        </a>
                        <div class="media-body">
                            <h3 class="t-12-dt f-roboto-b mb-10">
                                <a href="<?= Url::to(['/news/detail', 'slug1' => $news[2]['slug']]); ?>"
                                   class="link_unstyle"><?= BaseService::SplitText($news[2]['title'],$text_length) ?>
                                    <?php if ($news[2]['type_news'] == 1): ?><i class="fa fa-camera cl-999999"
                                                                                aria-hidden="true"></i><?php endif; ?>
                                </a>
                            </h3>
                        </div>
                    </div>
                </div>
                <?php if (isset($news[3])): ?>
                    <div class="col-md-6">
                        <ul class="list-post-global list-home-mb">
                            <?php foreach ($news as $k => $item) : if ($k > 2): ?>
                                <li class="<?=($k > 4)?'ispc':''?>">
                                    <a href="<?= URl::to(['news/detail','slug1'=>$item['slug']])?>" class="d-block ismb">
                                        <?= Html::img(\Yii::$app->params['mediaUrl'] . $item["images"], ['alt' => $item['title'], 'class' => 'mr-10', 'width' => 100]) ?>
                                    </a>
                                    <a href="<?= Url::to(['/news/detail', 'slug1' => $item['slug']]); ?>"
                                       class="link_unstyle f-roboto-b t-12-dt"><?= BaseService::SplitText($item['title'],$text_length) ?><?php if ($news[1]['type_news'] == 1): ?> <i class="fa fa-camera cl-999999"
                                                                                                                                             aria-hidden="true"></i><?php endif; ?>
                                    </a>
                                    <div style="clear:both" class="ismb"></div>
                                </li>
                            <?php endif;endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>
    </div>
    </div>
<?php endif; ?>