<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\services\BaseService;
?>
<?php $news = $data["news"]; ?>
<div class="box-article-cat mb-15">
    <div class="box-article-cat_head mb-15">
        <h2 class="f-roboto-b t-20 title"><a href="<?php echo Url::to(['/category/index', 'slug' => $slug]); ?>"
                                             class="link_unstyle"><?= $name ?></a></h2>
        <ul class="cat-child">
            <?php
            if ($data["subcate"]): foreach ($data["subcate"] as $row):
                ?>
                <li>
                    <a href="<?= Url::to(['/category/index', 'slug' => $row["slug"], 'slugparent' => $slug]); ?>">
                        <?= $row["name"] ?>
                    </a>
                </li>
            <?php endforeach;endif; ?>
        </ul>
    </div>
    <?php if(isset($news[0])): ?>
    <div class="box-article-cat_content">
        <div class="media">
            <a href="<?= Url::to(['/news/detail', 'slug1' => $news[0]['slug']]); ?>" class="d-block">
                <?= Html::img(\Yii::$app->params['mediaUrl'] . $news[0]["images"], ['alt' => $news[0]['title'], 'class' => 'mr-10', 'width' => 146]) ?>
            </a>
            <div class="media-body">
                <h3 class="t-14 f-roboto-b mb-10"><a href="<?= Url::to(['/news/detail', 'slug1' => $news[0]['slug']]); ?>" class="link_unstyle"><?= BaseService::SplitText($news[0]['title'],80)?></a></h3>
                <?php if(isset($news[1])): ?>
                <ul class="list-post-global">
                    <?php foreach($news as $k=>$item): if($k>0): ?>
                    <li><a href="<?= Url::to(['news/detail','slug1'=>$item['slug']])?>" class="link_unstyle"><?= BaseService::SplitText($item['title'],80)?></a></li>
                    <?php endif;endforeach;?>
                </ul>
                <?php endif;?>
            </div>
        </div>
    </div>
    <?php endif;?>
</div>