<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\services\BaseService;
?>

<?php $news = $data["news"]; ?>
<div class="slide-articles mb-30">
    <div class="container">
        <h2 class="f-roboto-b t-20 title"><a href="<?php echo Url::to(['/category/index', 'slug' => $slug]); ?>"
                                             class="link_unstyle"><?= $name ?></a></h2>
    </div>
    <div class="bg-4a4a4a">
        <div class="container">
            <div class="pt-15 pb-15">
                <a href="<?= Url::to(['/news/detail', 'slug1' => $news[0]['slug']]); ?>" class="d-block mb-15">
                    <?= Html::img(\Yii::$app->params['mediaUrl'] . $news[0]["images"], ['alt' => $news[0]['title']) ?>
                </a>
                <h2 class="t-20 f-roboto-b mb-10 cl-fff">
                    <a href="<?= Url::to(['/news/detail', 'slug1' => $news[0]['slug']]); ?>" class="link_unstyle"><?= BaseService::SplitText($news[0]['title'],80) ?></a
                </h2>
                <?php if (isset($news[1])): ?>
                <ul class="list-post-mb border-top">
                    <?php foreach($news as $k=>$item) : if($k >0): ?>
                    <li>
                        <div class="media">
                            <a href="<?= Url::to(['news/detail','slug1'=>$item['slug']])?>" class="mr-10">
                                <?= Html::img(\Yii::$app->params['mediaUrl'] . $item["images"], ['alt' => $item['title']]) ?>
                            </a>
                            <div class="media-body">
                                <h3 class="t-14 f-roboto-b cl-fff">
                                    <a href="<?= Url::to(['news/detail','slug1'=>$item['slug']])?>" class="link_unstyle"><?= BaseService::SplitText($item['title'],80) ?></a>
                                </h3>
                            </div>
                        </div>
                    </li>
                    <?php endif;endforeach;?>
                </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>