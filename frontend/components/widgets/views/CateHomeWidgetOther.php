<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\services\BaseService;
?>
<?php $news = $data["news"];?>
<div class="slide-articles mb-6 bg-fff">
    <div class="bg-e1e1e1"><h2 class="f-roboto-b t-14 title"><a href="<?php echo Url::to(['/category/index', 'slug' => $slug]); ?>" class="link_unstyle"><?= $name ?></a></h2></div>
    <?php if (isset($news[0])): ?>
    <div class="js-slide-articles-slider owl-carousel mb-6">
        <?php $totalItem = ceil(count($news) / 3);$indexStart = 0; ?>
        <?php for($i=0;$i<$totalItem;$i++):?>
        <div class="item">
            <?php for($j=$indexStart;$j<$indexStart+3;$j++): if(isset($news[$j])):?>
            <div class="mb-10">
                <a href="<?= Url::to(['news/index','slug1'=>$news[$j]['slug']])?>" class="d-block mb-15">
                    <?= Html::img(\Yii::$app->params['mediaUrl'] . $news[$j]["images"], ['alt' => $news[$j]['title'], 'class' => 'w-100']) ?>
                <h3 class="f-roboto-b t-15"><a href="<?= Url::to(['news/index','slug1'=>$news[$j]['slug']])?>" class="link_unstyle"><?= BaseService::SplitText($news[$j]['title'],80)?></a></h3>
            </div>
            <?php endif;endfor; ?>
        </div>
            <?php $indexStart = 3*($i+1);?>
         <?php endfor; ?>
    </div>
     <?php endif; ?>
</div>
<?php if (isset($news[0])): ?>
    <div class="d-flex justify-content-center mb-6">
        <button class="btn-nav-video btn-nav-video-left btn-nav-slide-articles-left"></button>
        <button class="btn-nav-video btn-nav-video-right btn-nav-slide-articles-right"></button>
    </div>
<?php endif; ?>
