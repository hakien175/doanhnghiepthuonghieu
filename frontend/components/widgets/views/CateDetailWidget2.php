<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\services\BaseService;
?>
<?php $news = $data["news"]; ?>
<div class="list-post-detail mb-10">
    <h2 class="t-14 f-roboto-b title">
        <a href="<?php echo Url::to(['/category/index', 'slug' => $slug]); ?>"
           class="link_unstyle"><?= $name ?></a>
    </h2>
    <?php if(isset($news[0])): ?>
    <ul class="list-post">
        <li>
            <div class="media">
                <a href="<?= Url::to(['/news/detail', 'slug1' => $news[0]['slug']]); ?>" class="d-block">
                    <?= Html::img(\Yii::$app->params['mediaUrl'] . $news[0]["images"], ['alt' => $news[0]['title'], 'class' => 'mr-5px', 'width' => 100]) ?>
                </a>
                <div class="media-body">
                    <h3 class="t-14 f-roboto-b mb-1"><a href="<?= Url::to(['/news/detail', 'slug1' => $news[0]['slug']]); ?>" class="link_unstyle"><?= BaseService::SplitText($news[0]['title'],90)?></a></h3>
                    <div class="cl-737373 t-11 d-flex align-items-center">
                        <img src="../images/icon-clock.png" alt=""
                             class="mr-1"><span><?= $news[0]['created_at']?></span>
                    </div>
                </div>
            </div>
        </li>
        <?php if(isset($news[1])):foreach($news as $k=>$item): if($k>0): ?>
        <li>
            <h3 class="t-14 f-roboto-b t-17-mb"><a href="<?= Url::to(['/news/detail', 'slug1' => $item['slug']]); ?>" class="link_unstyle"><?= BaseService::SplitText($item['title'],90)?></a></h3>
        </li>
       <?php endif;endforeach;endif;?>
    </ul>
    <?php endif;?>
</div>