<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\services\BaseService;
?>
<?php $news = $data["news"]; ?>
<div class="list-post-detail mb-10">
    <h2 class="t-14 f-roboto-b title"><a href="<?php echo Url::to(['/category/index', 'slug' => $slug]); ?>"
                                         class="link_unstyle"><?= $name ?></a></h2>
    <?php if(isset($news[0])): ?>
    <ul class="list-post">
        <?php foreach($news as $k=>$item): ?>
        <li>
            <a href="<?= Url::to(['/news/detail', 'slug1' => $item['slug']]); ?>" class="d-block mb-10">
                <?= Html::img(\Yii::$app->params['mediaUrl'] . $item["images"], ['alt' => $item['title'], 'class' => 'w-100']) ?>
            </a>
            <h3 class="t-14 f-roboto-b t-17-mb"><a href="<?= Url::to(['/news/detail', 'slug1' => $item['slug']]); ?>" class="link_unstyle"><?= BaseService::SplitText($item['title'],90)?></a></h3>
        </li>
        <?php endforeach;?>
    </ul>
    <?php endif;?>
</div>