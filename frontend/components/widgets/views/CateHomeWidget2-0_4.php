<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\services\BaseService;
?>

<?php $news = $data["news"]; ?>
<div class="box-article-cat mb-15">
    <div class="box-article-cat_head mb-15">
        <h2 class="f-roboto-b t-20 title"><a href="<?php echo Url::to(['/category/index', 'slug' => $slug]); ?>"
                                             class="link_unstyle"><?= $name ?></a></h2>
        <ul class="cat-child">
            <?php
            if ($data["subcate"]): foreach ($data["subcate"] as $row):
                ?>
                <li>
                    <a href="<?= Url::to(['/category/index', 'slug' => $row["slug"], 'slugparent' => $slug]); ?>">
                        <?= $row["name"] ?>
                    </a>
                </li>
            <?php endforeach;endif; ?>
        </ul>
    </div>
    <?php if(isset($news[0])): ?>
    <div class="box-article-cat_content">
        <div class="row gutter-10">
            <?php foreach($news as $k=>$item): if($k < 2): ?>
            <div class="col-md-6 mb-10">
                <a href="<?= Url::to(['/news/detail', 'slug1' => $item['slug']]); ?>" class="d-block mb-10">
                    <?= Html::img(\Yii::$app->params['mediaUrl'] . $item["images"], ['alt' => $item['title'], 'class' => 'w-100', 'width' => 146]) ?>
                </a>
                <h3 class="f-roboto-b t-14 t-17-mb"><a href="<?= Url::to(['/news/detail', 'slug1' => $item['slug']]); ?>" class="link_unstyle"><?= BaseService::SplitText($item['title'],80)?></a></h3>
            </div>
            <?php endif;endforeach;?>
        </div>
        <?php if(isset($news[2])): ?>
        <ul class="list-post-global pt-10 mb-15 border-top-e4e4e5">
            <?php foreach($news as $k=>$item): if($k > 1): ?>
            <li><a href="<?= Url::to(['news/detail','slug1'=>$item['slug']])?>" class="link_unstyle f-roboto-b"><?= $item['title']?></a></li>
            <?php endif;endforeach;?>
        </ul>
        <?php endif;?>
    </div>
    <?php endif;?>
</div>