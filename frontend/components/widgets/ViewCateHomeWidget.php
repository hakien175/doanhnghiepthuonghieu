<?php
namespace app\components\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\search\NewsSearch;


class ViewCateHomeWidget extends Widget{
	
	public $item;
	public $style;
    public $cate;
    public $slug;
    public $name;
//	
	public function init(){
		parent::init();		
	}
	
	public function run(){
            
            $News = new NewsSearch();
            
            $data = $News->getNewsHomeByCate($this->cate,$this->item);
//            var_dump($data); die;
            return $this->render($this->style,["data"=>$data,"name"=>$this->name,"slug"=>$this->slug,'News'=>$News]);
	}
}
?>