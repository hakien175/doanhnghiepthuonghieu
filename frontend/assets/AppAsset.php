<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/owl.carousel.min.css',
        'css/font-awesome.min.css',
        'css/bootstrap.min.css',
        'css/style.css?v=7',
        'css/custom.css?v=13',
    ];
    public $js = [
//        'js/jquery-3.3.1.min.js',
        'js/popper.min.js',
        'js/bootstrap.min.js',
        'js/owl.carousel.js',
        'js/comment.js',
        'js/global.js?v=5',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
