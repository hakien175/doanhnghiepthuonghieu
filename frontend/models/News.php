<?php

namespace frontend\models;

use Yii;
use yii\imagine\Image;
use yii\data\Pagination;
date_default_timezone_set("Asia/Vientiane");

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property string $content
 * @property string $images
 * @property integer $type
 * @property integer $cat_id
 * @property integer $slide_home
 * @property integer $user_id
 * @property string $desc_seo
 * @property string $title_seo
 * @property string $keywords
 * @property integer $public_date
 * @property string $source
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $note
 * @property integer $view
 * @property integer $public
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'link_video','type_videos', 'images', 'hot','community', 'cat_id', 'slide_home', 'status','view', 'public','type_news','show_comment'], 'required'],
            
            [['description', 'content','type_videos', 'desc_seo', 'title_seo', 'note'], 'string'],
            [['hot','community','type_focus','type_news', 'cat_id', 'slide_home', 'user_id', 'public_date', 'status', 'view', 'public','show_comment'], 'integer'],
              // [['slug'], 'unique'],
             // [['title'], 'unique','message'=>'Đường dẫn không được trùng'],
            [['created_at', 'updated_at'], 'safe'],
            [['keywords'],'safe'],
            [['link_video'],'safe'],
            [['title', 'slug', 'images', 'source'], 'string', 'max' => 255],
            [['file'],'file','extensions'=>'mp4,flv,MKV'],
             
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Tiêu đề',
            'slug' => 'Đường dẫn tĩnh',
            'description' => 'Mô tả',
            'content' => 'Nội dung',
            'images' => 'Hình ảnh',
            'hot' => 'Tin hot',
            'community' => 'Tin cộng đồng',
            'cat_id' => 'Danh mục cha',
            'slide_home' => 'Kiểu hiển thị trang chủ',
            'user_id' => 'Người tạo',
            'desc_seo' => 'Mô tả seo',
            'title_seo' => 'Tiêu đề seo',
            'public_date' => 'Hẹn giờ đăng',
            'source' => 'Nguồn',
            'status' => 'Trạng thái',
            'created_at' => 'Ngày tạo',
            'updated_at' => 'Ngày cập nhật',
            'note' => 'Ghi chú',
            'view' => 'Số lượt xem',
            'public' => 'Trạng thái bài',
            'type_focus' => 'Tiêu điểm',
            'keywords' => 'Keywords',
            'link_video' => 'Link video',
            'type_videos' => 'Chọn kiểu video upload',
            'type_news' => 'Chọn loại tin',
        ];
    }
    public function getCommentbls(){
        return $this->hasMany(Comment::className(),['news_id' => 'id']);

    }
    public function getNewnames(){
        return $this->hasOne(Category::className(),['id' => 'cat_id']);

    }
    public function getNameuser(){
        return $this->hasOne(User::className(),['id' => 'user_id']);

    }
       public function saveData($model, $post) {
        /* @var $model app\modules\app90phut\models\News */
        if ($model->images != null && $model->images != "") {
            $string_img = $post["News"]["images"];
            // $postcat = $post["News"]["cat_id"];
            // $cat = Category::findOne(['id'=>$postcat]);
            
            // $cat1 = Category::findOne(['id'=>$cat->parent]);

            $filename = $this->uploadBase64("uploads/images/", $string_img, 460, 300);
            if ($filename) {

                $model->images = $filename;
        
            }
          
        }
          return 0;
    }

    public function uploadBase64($path, $base64Img, $width, $height) {
        $strSplit = explode("}}}", $base64Img);
        if (count($strSplit) == 2) {
            $fileNameAll = $strSplit[0];
            
            $extFile = substr($fileNameAll, strrpos($fileNameAll, '.') + 1);
            $fileName = str_replace("." . $extFile, "", $fileNameAll) . time() . rand(0, 10) . "." . $extFile;
            
            $strBase64Img = explode("base64,", $strSplit[1]);
           // echo $strBase64Img[1]; die;
            $base64Decode = base64_decode($strBase64Img[1]);

            
          
            $this->mkdir(upload_path.'/'.$path . '/');
            $path = $path . date("Y/m");
            
            $path = upload_path.'/'.$path .'/';
            //$path = str_replace('/','\\', $path);

         // echo $path  . $fileName;die;

            $filegetimg = explode(".",$fileName);
            
            $ext =  $filegetimg[count($filegetimg)-1];

            unset($filegetimg[count($filegetimg)-1]);

             $fileName = $this->makeAlias(implode(".", $filegetimg));



             $fileName = $fileName.'.'.$ext;



            file_put_contents($path  . $fileName, $base64Decode);
           
            $file = $path . "/" . $fileName;

             if ($width < 99999) {
                Image::thumbnail($file, $width, $height)->save($file, ['quality' => 100]);
            }
            return "uploads/images/".date("Y/m").'/'.$fileName;

        } else
            return false;
    }

      public function mkdir($path) {
//        echo $path; die;
        if (!is_dir($path . date("Y"))) {
            mkdir($path . date("Y"), 0777, true);
        }

        if (!is_dir($path . date("Y/m"))) {
            mkdir($path . date("Y/m"), 0777, true);
        }

        // if (!is_dir($path . date("Y/m/d"))) {
        //     mkdir($path . date("Y/m/d"), 0777, true);
        // }


//            $this->chmod($path);
    }
    public function getReadweek(){
         date_default_timezone_set('Asia/Ho_Chi_Minh');
         $today = date('Y-m-d H:i:s') ;
         $week = strtotime(date("Y-m-d", strtotime($today)) . " -1 week");
         $week = strftime("%Y-%m-%d",$week);
         // echo $week; 
         $readweek = News::find()->where(['status'=>8])->andWhere(['between','created_at',$week,$today])->andWhere(['public'=>1])->limit(5)->OrderBy('view DESC')->all();
         return $readweek;
        
 
    }
    public function getReadweek1(){
         date_default_timezone_set('Asia/Ho_Chi_Minh');
         $today = date('Y-m-d H:i:s') ;
         $week = strtotime(date("Y-m-d", strtotime($today)) . " -1 week");
         $week = strftime("%Y-%m-%d",$week); 
         $readweek = News::find()->where(['status'=>8])->andWhere(['cat_id'=>68])->andWhere(['between','created_at',$week,$today])->andWhere(['public'=>1])->limit(4)->OrderBy('view DESC')->all();
         return $readweek;
        
 
    }
    public function getReadmonth(){
         date_default_timezone_set('Asia/Ho_Chi_Minh');
         $today = date('Y-m-d H:i:s');
         $month = strtotime(date("Y-m-d", strtotime($today)) . " -1 month");
         $month = strftime("%Y-%m-%d",$month); 
         $readmonth = News::find()->where(['status'=>8])->andWhere(['between','created_at',$month,$today])->andWhere(['public'=>1])->limit(10)->OrderBy('view DESC')->all();
         return $readmonth;
    }
    public function getSearch($limlit){



        $searchnews = News::find()->where(['LIKE','title',Yii::$app->request->get('keywords')])->AndWhere(['status'=>8])->andWhere(['public'=>1]);
        
        
        $count = $searchnews->count();
          $pages = new Pagination(['totalCount' => $count,'defaultPageSize' => $limlit]);
          $searchnews = $searchnews->offset($pages->offset)
        ->limit($pages->limit)
        ->all();

            $data = [
                'pages' =>$pages,
                'searchnews' =>$searchnews
            ];
            // echo "<pre>";
            // var_dump($data);
            // echo "</pre>";
        return $data;

    }
     public function getKeywordsid(){
        return $this->hasMany(NewKeyword::className(),['news_id'=>'id']);
    }
    public function getCommentid(){
        return $this->hasMany(Comment::className(),['news_id'=>'id'])->where(['parent'=>0])->andWhere(['status'=>1]);
    }
    public function getCountcomment(){
        return $this->hasMany(Comment::className(),['news_id'=>'id'])->count();
    }



    public function makeAlias($str, $lowerCase = true) {
        $search = array(
            '#(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)#',
            '#(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)#',
            '#(ì|í|ị|ỉ|ĩ)#',
            '#(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)#',
            '#(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)#',
            '#(ỳ|ý|ỵ|ỷ|ỹ)#',
            '#(đ)#',
            '#(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)#',
            '#(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)#',
            '#(Ì|Í|Ị|Ỉ|Ĩ)#',
            '#(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)#',
            '#(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)#',
            '#(Ỳ|Ý|Ỵ|Ỷ|Ỹ)#',
            '#(Đ)#',
            "/[^a-zA-Z0-9\-\_]/",
        );
        $replace = array(
            'a',
            'e',
            'i',
            'o',
            'u',
            'y',
            'd',
            'A',
            'E',
            'I',
            'O',
            'U',
            'Y',
            'D',
            '-',
        );
        $str = preg_replace($search, $replace, $str);
        $str = preg_replace('/(-)+/', '-', $str);
        $str = preg_replace('/^-+|-+$/', '', $str);
        if ($lowerCase)
            $str = strtolower($str);
        return $str;
    }
    
    
    function catchuoi($chuoi,$gioihan){ 

        if(strlen($chuoi)<=$gioihan) 
        { 
            return $chuoi; 
        } 
        else{ 
            $new_txt =substr($chuoi,0,$gioihan); 
            $new_txt =substr($chuoi,0,strrpos($new_txt," ")); 

            return $new_txt.'...' ; 
        }
    }

    public function saveLog($action,$content)
    {
      $path_link = upload_path.'/savelog/'.date('Y-m-d').'.txt';
       if(!Yii::$app->user->isGuest){
           $content = Yii::$app->user->identity->username.'-'.$action.':'.$content.PHP_EOL;
       }
     
      file_put_contents($path_link,$content,FILE_APPEND);
    }


  


   

 
}
