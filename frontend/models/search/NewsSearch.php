<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\News;
use frontend\models\Category;

/**
 * NewsSearch represents the model behind the search form about `backend\models\News`.
 */
class NewsSearch extends News
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type','type_focus', 'cat_id', 'slide_home', 'user_id', 'public_date', 'status', 'view', 'public'], 'integer'],
            [['title', 'slug', 'description', 'content', 'images', 'desc_seo', 'title_seo', 'source', 'created_at', 'updated_at', 'note'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = News::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'cat_id' => $this->cat_id,
            'slide_home' => $this->slide_home,
            'user_id' => $this->user_id,
            'public_date' => $this->public_date,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'view' => $this->view,
            'public' => $this->public,
            'type_focus' => $this->type_focus,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'images', $this->images])
            ->andFilterWhere(['like', 'desc_seo', $this->desc_seo])
            ->andFilterWhere(['like', 'title_seo', $this->title_seo])
            ->andFilterWhere(['like', 'source', $this->source])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }

    public function getNewsHomeByCate($cate,$item){

        $sub_cate  = $this->getAllSubCate($cate);

        $all_cate = '';
        foreach ($sub_cate as $row) {
            $all_cate = $all_cate.$row['id'].',';
        }

        if($all_cate=='')
        {
                $all_cate = "$cate";
        }else{
            $all_cate = $all_cate.$cate;
        }

        // var_dump(explode(",", $all_cate)); die;
        $news = News::find()
            ->select(['title','news.created_at AS created_at','type_news','category.name AS cat_name','category.slug AS cat_slug','news.slug AS slug','view','description','images','user_id'])
            ->leftJoin('category', 'category.id = news.cat_id')
            ->where(['news.cat_id'=>explode(",", $all_cate)])
            ->andWhere(['news.status'=>8])->andWhere(['news.public'=>1])
            ->limit($item)->OrderBy('news.id DESC')->asArray()->all();

        $data['news'] = $news;
        $data['subcate'] = $sub_cate;

        return $data;
        
    }

    public function getAllSubCate($cate){

        return Category::find()->select(['id','name','slug'])->where(['parent'=>$cate])->asArray()->all();
    }
    function catchuoi($chuoi,$gioihan){ 

        if(strlen($chuoi)<=$gioihan) 
        { 
            return $chuoi; 
        } 
        else{ 
            $new_txt =substr($chuoi,0,$gioihan); 
            $new_txt =substr($chuoi,0,strrpos($new_txt," ")); 

            return $new_txt.'...' ; 
        }
    }

}
