<?php

namespace frontend\models;

use Yii;
use frontend\models\News;
use frontend\models\Category;
/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property integer $parent
 * @property string $desc_seo
 * @property string $title_seo
 * @property string $keywords
 * @property integer $show_home
 * @property integer $orders_sort
 * @property integer $status
 * @property string $created_at
 * @property string $update_at
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    private $_cats = [];
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'title_seo', 'show_home', 'orders_sort', 'status'], 'required'],
            [['parent', 'show_home', 'orders_sort', 'status'], 'integer'],
            [['desc_seo', 'title_seo', 'keywords'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên',
            'slug' => 'Đường dẫn thân thiện',
            'parent' => 'Danh mục',
            'desc_seo' => 'Desc Seo',
            'title_seo' => 'Title Seo',
            'keywords' => 'Keywords',
            'show_home' => 'Show Home',
            'orders_sort' => 'Sắp xếp danh mục',
            'status' => 'Trang thái',
            'created_at' => 'Ngày tạo',
            'updated_at' => 'Ngày cập nhật',
        ];
    }
   

     public function getNews(){
        return $this->hasMany(News::className(),['cat_id'=>'id']);
    }




}
