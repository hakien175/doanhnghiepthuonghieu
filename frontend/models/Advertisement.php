<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "advertisement".
 *
 * @property integer $id
 * @property string $name
 * @property string $link_advertisement
 * @property integer $type
 * @property string $size
 * @property integer $active
 */
class Advertisement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'advertisement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'link_advertisement', 'type', 'active'], 'required','message'=>'Trường này không được để trống'],
            [['link_advertisement'], 'string'],
            [['type', 'active'], 'integer'],
            [['name', 'size'], 'string', 'max' => 255,'message'=>'không được quá 255 ký tự'],
            [['file'],'file','extensions'=>'jpg,png,gif,swf,JPG,PNG,GIF,SWF'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên',
            'link_advertisement' => 'Linh quảng cáo',
            'type' => 'Chọn kiểu quảng cáo',
            'size' => 'kích cỡ quảng cáo',
            'active' => 'Trạng thái',
        ];
    }
}
