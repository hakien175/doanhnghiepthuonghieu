<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\services;

use app\models\News;
use Yii;

/**
 * Description of NewsService
 *
 * @author dongh
 */
class BaseService
{

    public static function makeAlias($str, $lowerCase = true)
    {
        $search = array(
            '#(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)#',
            '#(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)#',
            '#(ì|í|ị|ỉ|ĩ)#',
            '#(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)#',
            '#(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)#',
            '#(ỳ|ý|ỵ|ỷ|ỹ)#',
            '#(đ)#',
            '#(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)#',
            '#(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)#',
            '#(Ì|Í|Ị|Ỉ|Ĩ)#',
            '#(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)#',
            '#(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)#',
            '#(Ỳ|Ý|Ỵ|Ỷ|Ỹ)#',
            '#(Đ)#',
            "/[^a-zA-Z0-9\-\_]/",
        );
        $replace = array(
            'a',
            'e',
            'i',
            'o',
            'u',
            'y',
            'd',
            'A',
            'E',
            'I',
            'O',
            'U',
            'Y',
            'D',
            '-',
        );
        $str = preg_replace($search, $replace, $str);
        $str = preg_replace('/(-)+/', '-', $str);
        $str = preg_replace('/^-+|-+$/', '', $str);
        if ($lowerCase)
            $str = strtolower($str);
        return $str;
    }

    public static function MakeLink($news, $pslug)
    {

        return Yii::$app->params['baseUrl'] . BaseService::makeAlias($title) . '-p' . $id . '.html';
    }

    public static function MakeLinkCate($name, $id)
    {
        return Yii::$app->params['baseUrl'] . '/category/' . BaseService::makeAlias($name) . '-' . $id . '.html';
    }

    public static function SplitText($chuoi, $gioihan)
    {

        if (strlen($chuoi) <= $gioihan) {
            return $chuoi;
        } else {
            $new_txt = substr($chuoi, 0, $gioihan);
            $new_txt = substr($chuoi, 0, strrpos($new_txt, " "));

            return $new_txt . '...';
        }

    }

    public static function getDayInWeek()
    {

        $weekday = date('w');
        switch ($weekday) {
            case '1':
                $weekday = 'Thứ hai';
                break;
            case '2':
                $weekday = 'Thứ ba';
                break;
            case '3':
                $weekday = 'Thứ tư';
                break;
            case '4':
                $weekday = 'Thứ năm';
                break;
            case '5':
                $weekday = 'Thứ sáu';
                break;
            case '6':
                $weekday = 'Thứ bảy';
                break;
            default:
                $weekday = 'Chủ nhật';
                break;
        }
        return $weekday;

    }

    public static function detectMobile()
    {
        $browserType = "";
        $tablet_browser = 0;
        $mobile_browser = 0;
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
                $tablet_browser++;
            }

            if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile|bb10)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
                $mobile_browser++;
            }

            if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
                $mobile_browser++;
            }

            $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
            $mobile_agents = array(
                'w3c ', 'acs-', 'alav', 'alca', 'amoi', 'audi', 'avan', 'benq', 'bird', 'blac',
                'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco', 'eric', 'hipt', 'inno',
                'ipaq', 'java', 'jigs', 'kddi', 'keji', 'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-',
                'maui', 'maxo', 'midp', 'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-',
                'newt', 'noki', 'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox',
                'qwap', 'sage', 'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar',
                'sie-', 'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-',
                'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi', 'wapp',
                'wapr', 'webc', 'winw', 'winw', 'xda ', 'xda-', 'BlackBerry', 'BB10', 'RIM Tablet');

            if (in_array($mobile_ua, $mobile_agents)) {
                $mobile_browser++;
            }

            if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'opera mini') > 0) {
                $mobile_browser++;
                //Check for tablets on opera mini alternative headers
                $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA']) ? $_SERVER['HTTP_X_OPERAMINI_PHONE_UA'] : (isset($_SERVER['HTTP_DEVICE_STOCK_UA']) ? $_SERVER['HTTP_DEVICE_STOCK_UA'] : ''));
                if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
                    $tablet_browser++;
                }
            }

            if ($tablet_browser > 0) {
                $browserType = "talbe";
            } else if ($mobile_browser > 0) {
                // do something for mobile devices
                $browserType = "mobile";
            } else {
                // do something for everything else
                $browserType = "desktop";
            }
        }
        return $browserType;
    }

    public static  function getInfoWeather($arrWeather, $province)
    {
        if (!is_array($arrWeather) || $province == "")
            return false;
        if (!array_key_exists($province, $arrWeather))
            return false;
        $dirWeather = "weather";
        if (is_dir($dirWeather) === false) {
            mkdir($dirWeather, 0777, true);
        }
        foreach ($arrWeather as $province2 => $city_id) {
            $nameWeatherToday = "weather_" . date("d") . date("m") . date("Y") . "_$province2.json";
            $timeYesterday = time() - 24 * 3600;
            $nameWeatherYesterday = "weather_" . date("d", $timeYesterday) . date("m", $timeYesterday) . date("Y", $timeYesterday) . "_$province2.json";

            // delete json weather yesterday
            if (file_exists("weather" . "/" . $nameWeatherYesterday))
                unlink("weather" . "/" . $nameWeatherYesterday);
            // save json weather today.
            if (!file_exists($dirWeather . "/" .$nameWeatherToday)) {
                $file = fopen($dirWeather . "/" . $nameWeatherToday, "a");
                $urlApiWeather = "https://api.weatherbit.io/v2.0/forecast/daily?days=7&lang=en&city_id=$city_id&key=f5c39bde034840d888e43ec7f5ac5264";
                $jsonWeather = file_get_contents($urlApiWeather);
                fwrite($file, $jsonWeather);
                fclose($file);
            }
        }
        $nameWeatherCurrentToday =  "weather_" . date("d") . date("m") . date("Y") . "_$province.json";
        $jsonWeather =  fopen($dirWeather . "/" . $nameWeatherCurrentToday,"r");
        $dataFile = fgets($jsonWeather);
        $dataFile2 = json_decode($dataFile);
        return [
            "low_temp"=>$dataFile2->data[0]->low_temp,
            "high_temp"=>$dataFile2->data[0]->high_temp,
        ];
    }

}
