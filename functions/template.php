<?php
function get_header()
{
    if (file_exists("layout/header.html"))
        include 'layout/header.html';
}

function get_footer()
{
    if (file_exists("layout/footer.html"))
        include 'layout/footer.html';
}

function get_layout_main($path)
{
    if (file_exists("$path"))
        include "$path";
}

?>